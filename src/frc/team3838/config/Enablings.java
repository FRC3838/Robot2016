package frc.team3838.config;

/**
 * Mapping/Configuration of enabled components, primarily subsystems.
 */
// @formatter:off
public class Enablings
{
    public static boolean isDriveTrainSubsystemEnabled = true;
    public static boolean isNavxSubsystemEnabled = true;
    public static boolean isBallSubsystemEnabled = true;
    public static boolean isArmsSubsystemEnabled = true;
    public static boolean isUsbCameraSubsystemEnabled = true;
    public static boolean isWinchSubsystemEnabled = true;
    public static boolean isMonitoringSubsystemEnabled = true;

    public static boolean isBallPidSubsystemEnabled = false;

    public static boolean isGyroAccelSubsystemEnabled = false;
    public static boolean isSonarSubsystemEnabled = false;
    public static boolean isDioTesterSubsystemEnabled = false;

    private Enablings() { }
}
// @formatter:on~