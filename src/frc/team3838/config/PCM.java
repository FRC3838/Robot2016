package frc.team3838.config;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.common.collect.ImmutableSortedMap;



/**
 * Port assignments for the Pneumatic Control Module (PCM).
 * [Not to be confused with the PWMs (Pulse Width Modulation) channel assignment class.]
 */
public final class PCM
{



//    public static final int leftSolenoidA = 0;
//    public static final int leftSolenoidB = 1;
//    public static final int rightSolenoidA = 2;
//    public static final int rightSolenoidB = 3;





    public static final ImmutableSortedMap<Integer, String> mappings;

    private static final Logger logger = LoggerFactory.getLogger(PCM.class);


    static
    {
        logger.info("Mapping PCM assignments and checking for duplicates");
        Map<Integer, String> tempMap = new HashMap<>();
        try
        {
            final Class<PCM> clazz = PCM.class;
            final Field[] fields = clazz.getFields();

            for (Field field : fields)
            {
                if (int.class.isAssignableFrom(field.getType()))
                {
                    final int port = field.getInt(null);
                    if (port != -1)
                    {
                        if (tempMap.containsKey(port))
                        {
                            final String msg = String.format(">>>>>FATAL ERROR : PCM port # %s is duplicated! It is assigned to both '%s' and '%s'", port, tempMap.get(port), field.getName());
                            logger.error(">>>>>FATAL ERROR<<<<<");
                            logger.error(msg);
                            logger.error(">>>>>FATAL ERROR<<<<<");
                            throw new ExceptionInInitializerError(msg);
                        }
                        else
                        {
                            tempMap.put(port, field.getName());
                        }
                    }
                }
            }
        }
        catch (Exception e)
        {
            //noinspection UnnecessaryToStringCall
            logger.warn("Could not validate the PCM fields due to an Exception. Cause Details: {}", e.toString(), e);
            tempMap = new HashMap<>();
        }
        mappings = ImmutableSortedMap.copyOf(tempMap);
    }


    private PCM() { }


    public static void init()
    {
        // a no op method to have the PCM class initialize
    }
}
