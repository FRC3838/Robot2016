package frc.team3838.config;


import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.common.collect.ImmutableSortedMap;



/** Configuration 'constants' class for Analog I/O (AIO) port assignments. */
@SuppressWarnings("PointlessArithmeticExpression")
public final class AIOs
{

    private static final Logger logger = LoggerFactory.getLogger(AIOs.class);

    /**
     * Factor added to port # on the MXP (myRIO Expansion Port) 'More Board'. Thus for
     * AIO port 2 on the MXP More board is AIO 12 (2 + ADD_FACTOR) to the roboRIO.<br/>
     * See <a href='http://www.revrobotics.com/product/more-board/'>http://www.revrobotics.com/product/more-board/</a><br/>
     * See <a href='http://wpilib.screenstepslive.com/s/4485/m/13809/l/145309-java-conventions-for-objects-methods-and-variables#MXPIONumbering'>http://wpilib.screenstepslive.com/s/4485/m/13809/l/145309-java-conventions-for-objects-methods-and-variables#MXPIONumbering</a><br/>
     */
    public static final int MXP_ADD_FACTOR = 10; //This may actually be '4

//    public static final int GYRO = 0;

    public static final int ANALOG_ULTRASONIC = 0;

    public static final int ARM_MONITOR_1 = 1;
    public static final int ARM_MONITOR_2 = 2;


    public static final ImmutableSortedMap<Integer, String> mappings;


    static
    {
        logger.info("Mapping AIO assignments and checking for duplicates");
        Map<Integer, String> tempMap = new HashMap<>();
        try
        {
            final Class<AIOs> clazz = AIOs.class;
            final Field[] fields = clazz.getFields();

            for (Field field : fields)
            {
                if (int.class.isAssignableFrom(field.getType()) && !"MXP_ADD_FACTOR".equals(field.getName()))
                {
                    final int port = field.getInt(null);
                    if (port != -1)
                    {
                        if (tempMap.containsKey(port))
                        {
                            final String msg = String.format(">>>>>FATAL ERROR : AIO port # %s is duplicated! It is assigned to both '%s' and '%s'", port, tempMap.get(port), field.getName());
                            logger.error(">>>>>FATAL ERROR<<<<<");
                            logger.error(msg);
                            logger.error(">>>>>FATAL ERROR<<<<<");
                            throw new ExceptionInInitializerError(msg);
                        }
                        else
                        {
                            tempMap.put(port, field.getName());
                        }
                    }
                }
            }
        }
        catch (Exception e)
        {
            //noinspection UnnecessaryToStringCall
            logger.warn("Could not validate the AIO fields due to an Exception. Cause Details: {}", e.toString(), e);
            tempMap = new HashMap<>();
        }
        mappings = ImmutableSortedMap.copyOf(tempMap);
    }


    private AIOs() { }


    public static void init()
    {
        // a no op method to have the AIOs class initialize
    }
}
