package frc.team3838.subsystems;


import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import edu.wpi.first.wpilibj.ADXL362;
import edu.wpi.first.wpilibj.ADXL362.AllAxes;
import edu.wpi.first.wpilibj.ADXRS450_Gyro;
import edu.wpi.first.wpilibj.SPI;
import edu.wpi.first.wpilibj.SPI.Port;
import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.interfaces.Accelerometer.Range;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc.team3838.config.Enablings;
import frc.team3838.utils.MathUtils;
import frc.team3838.utils.logging.LogbackProgrammaticConfiguration;



public class GyroAccelSubsystem extends Subsystem
{
    @SuppressWarnings("UnusedDeclaration")
    private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(GyroAccelSubsystem.class);

    private ADXRS450_Gyro gyro;
    private ADXL362 accelerometer;




    /**
     * This is a private constructor to ensure a single instance (i.e. singleton pattern).
     * Use the static {@link #getInstance()} method to obtain a reference to the subsystem.
     * For example, instead of doing this:
     * <pre>
     *     GyroAccelSubsystem subsystem = new GyroAccelSubsystem();
     * </pre>
     * do this:
     * <pre>
     *     GyroAccelSubsystem subsystem = GyroAccelSubsystem.getInstance();
     * </pre>
     */
    private GyroAccelSubsystem()
    {
        if (isEnabled())
        {
            try
            {
                logger.debug("Initializing {}", getClass().getSimpleName());
                //If the board is not plugged in, an exception is not thrown. And there is no method to determine that the
                //object was not properly initialized. (An error message is sent to the Driver's station.)
                //The isDeviceConnected() method's logic  is extracted from the ADXRS450_Gyro class as a way to determine if the board is connected
                if (isDeviceConnected(Port.kOnboardCS0))
                {
                    gyro = new ADXRS450_Gyro();
                    //TODO: Determine is this range setting is OK / the one we want
                    accelerometer = new ADXL362(Range.k2G);
                    logger.debug("{} initialization completed successfully", getClass().getSimpleName());
                }
                else
                {
                    logger.warn("The GyroAccel (SPI) board is not connected. The {} will NOT be initialized and will be DISABLED.", getClass().getSimpleName());
                    Enablings.isGyroAccelSubsystemEnabled = false;
                }
            }
            catch (Exception e)
            {
                Enablings.isGyroAccelSubsystemEnabled = false;
                logger.error("An exception occurred in {} constructor", getClass().getSimpleName(), e);
            }
        }
        else
        {
            logger.warn("{} is disabled and will not be initialized", getClass().getSimpleName());
        }
    }


    /**
     * Determines if an device is connected to the specified SPI port.
     * It was found that with the GyroAccel SPI board that if the board is not plugged in, an exception
     * is NOT thrown by the constructor. And there is no method to determine that the
     * object was not properly initialized. (An error message is sent to the Driver's station.)
     * The following logic is extracted from the ADXRS450_Gyro class as a way to determine if a device
     * is connected. <strong>It can't guarantee it will work for all SPI devices and therefore should be
     * tested when used.</strong>
     * @param spiPort the port to check
     * @return if a device is connected to the SPI Port
     */
    private static boolean isDeviceConnected(Port spiPort)
    {
        final SPI spi = new SPI(spiPort);

        try
        {
            int cmdHi = 0x8000 | (0x0C << 1);
            int v = cmdHi;
            boolean parity = false;
            while (v != 0)
            {
                parity = !parity;
                v = v & (v - 1);
            }

            ByteBuffer buf = ByteBuffer.allocateDirect(4);
            buf.order(ByteOrder.BIG_ENDIAN);
            buf.put(0, (byte) (cmdHi >> 8));
            buf.put(1, (byte) (cmdHi & 0xff));
            buf.put(2, (byte) 0);
            buf.put(3, (byte) (parity ? 0 : 1));

            spi.write(buf, 4);
            spi.read(false, buf, 4);

            int result = ((buf.get(0) & 0xe0) == 0) ? 0 : ((buf.getInt(0) >> 5) & 0xffff);
            final boolean isDisconnected = (result & 0xff00) != 0x5200;
            return !isDisconnected;
        }
        finally
        {
            spi.free();
        }
    }


    @Override
    public void initDefaultCommand()
    {
//        setDefaultCommand(new GyroAccelReportingCommand());
    }


    public static boolean isEnabled()
    {
        return Enablings.isGyroAccelSubsystemEnabled;
    }

    public void reportValues()
    {
        if (isEnabled() && (gyro != null))
        {
            final double angle = gyro.getAngle();
            final double rate = gyro.getRate();
            final AllAxes accelerations = accelerometer.getAccelerations();
            final double xAxis = accelerations.XAxis;
            final double yAxis = accelerations.YAxis;
            final double zAxis = accelerations.ZAxis;

//        final String msg = String.format("GYRO angle: %5s rate: %5s,\t    ACCELEROMETER:  X: %5s Y: %5s Z: %5s",
//                                         MathUtils.formatNumber(angle, 1),
//                                         MathUtils.formatNumber(rate, 2),
//                                         MathUtils.formatNumber(xAxis, 2),
//                                         MathUtils.formatNumber(yAxis, 2),
//                                         MathUtils.formatNumber(zAxis, 2));
            if (LogbackProgrammaticConfiguration.IN_DEBUGGING_MODE)
            {
                SmartDashboard.putString("Gyro Angel", MathUtils.formatNumber(angle, 1));
                SmartDashboard.putString("Gyro Rate", MathUtils.formatNumber(rate, 3));
                SmartDashboard.putString("Gyro X Axis", MathUtils.formatNumber(xAxis, 3));
                SmartDashboard.putString("Gyro Y Axis", MathUtils.formatNumber(yAxis, 3));
                SmartDashboard.putString("Gyro Z Axis", MathUtils.formatNumber(zAxis, 31));
            }
        }
    }


    /**
     * Calibrates the Gyro if this subsystem is enabled.
     * @see {@link ADXRS450_Gyro#calibrate()}
     */
    public void calibrateGyro()
    {
        if (isEnabled())
        {
            logger.info("Starting Gyro calibration...");
            gyro.calibrate();
            logger.info("Gyro calibration completed");
        }
    }


    /**
     * Resets the Gyro if this subsystem is enabled.
     *
     * @see {@link ADXRS450_Gyro#reset()}
     */
    public void resetGyro()
    {
        if (isEnabled())
        {
            gyro.reset();
        }
    }


    /*******************************************************************************************************/
    /*  NO NON-SINGLETON INITIALIZATION CODE BELOW THIS POINT                                              */
    /*******************************************************************************************************/


    /**
     * *** THE SINGLETON FILED NEEDS TO BE LAST FIELD DEFINED ****<br />
     * So we put it at the end of the class to be sure
     * All static fields/properties need to be initialized prior to the constructor running
     * If the singleton field (or a static init block) is put in the middle of the field declarations
     * (which can happen when the IDE automatically creates new Fields at the end of the list) subtle
     * and very hard to track down bugs can be introduced.
     */
    private static final GyroAccelSubsystem singleton = new GyroAccelSubsystem();


    /**
     * Gets the single instance of this subsystem. Use of this method replaces the use
     * of a constructor such as <tt>new GyroAccelSubsystem()</tt> in order to use ensure
     * only a single instance of a Subsystem is created. This is known as a Singleton.
     * For example, instead of doing this:
     * <pre>
     *     GyroAccelSubsystem subsystem = new GyroAccelSubsystem();
     * </pre>
     * do this:
     * <pre>
     *     GyroAccelSubsystem subsystem = GyroAccelSubsystem.getInstance();
     * </pre>
     *
     * @return the single instance of this subsystem.
     */
    public static GyroAccelSubsystem getInstance() {return singleton;}

    /*******************************************************************************************************/
    /*  NO CODE BELOW THIS POINT                                                                           */
    /*******************************************************************************************************/

}
