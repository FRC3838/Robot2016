package frc.team3838.subsystems;


import javax.annotation.Nullable;

import org.apache.commons.lang3.tuple.ImmutablePair;

import edu.wpi.first.wpilibj.DigitalInput;
import edu.wpi.first.wpilibj.Encoder;
import edu.wpi.first.wpilibj.GenericHID;
import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.RobotDrive;
import edu.wpi.first.wpilibj.Victor;
import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc.team3838.commands.drive.DriveControlModeSelectionCommand;
import frc.team3838.config.DIOs;
import frc.team3838.config.Enablings;
import frc.team3838.config.PWMs;
import frc.team3838.config.SDBKeys;
import frc.team3838.controls.joysticks.F310GamePad.Axises;
import frc.team3838.controls.joysticks.Joysticks;
import frc.team3838.utils.MathUtils;
import frc.team3838.utils.logging.LogbackProgrammaticConfiguration;
import frc.team3838.utils.motors.MotorOps;



@SuppressWarnings("FieldCanBeLocal")
public class DriveTrainSubsystem extends Subsystem
{
    private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(DriveTrainSubsystem.class);

    /** If the joystick value is less than this value, it will be zeroed out. */
    public static final double JOYSTICK_DRIFT_CUT_OFF_VALUE = 0.09;
    
    public static final double STALL_SPEED = 0.57;
//    private static final double DISTANCE_PER_PULSE = 0.04488;
    // Modified 10/13/16 by KMS for 24 to 12 tooth gear change.
    private static final double DISTANCE_PER_PULSE = 0.04488 * 0.725514 * 0.500;

    //TODO: Add an ITableListener so this can be set via the Smartdashboard
    private int driveSensitivityCurvePower = 2;

    private Victor leftMotorController;
    private Victor rightMotorController;
    private MotorOps leftMotorOps;
    private MotorOps rightMotorOps;
    private RobotDrive robotDrive;
    private DigitalInput leftEncoderChADI;
    private DigitalInput leftEncoderChBDI;
    private DigitalInput rightEncoderChADI;
    private DigitalInput rightEncoderChBDI;
    @Nullable
    private Encoder leftEncoder;
    @Nullable
    private Encoder rightEncoder;

    private ControlMode controlMode = ControlMode.GAMEPAD_TANK;


    /**
     * The pull factor to <em>decrease</em> the LEFT motor compared to the right motor:
     * <pre>
     *     double rightSpeed = 0.6;
     *     double leftSpeed = rightSpeed * PULL_FACTOR; // 0.5797
     * </pre>
     */
    public static final double PULL_FACTOR = 0.966184;


    public static double readAutoSpeed() {return -MathUtils.constrainBetweenZeroAndOne(SmartDashboard.getNumber("Auto Speed Abs", 0.6));}


    static
    {
        if (LogbackProgrammaticConfiguration.IN_DEBUGGING_MODE)
        {
            SmartDashboard.putNumber("Auto Speed Abs", 0.6);
        }
    }
//    /**
//     * The pull factor to increase the RIGHT motor over the left motor:
//     * <pre>
//     *     double leftSpeed = 0.6;
//     *     double rightSpeed = leftSpeed * PULL_FACTOR; // 0.621
//     * </pre>
//     */
//    public static final double INVERSE_PULL_FACTOR2 = 1.035;




    public enum ControlMode
    {
        JOYSTICK_TANK("Joystick Tank Control"),
        JOYSTICK_ARCADE_RIGHT_STICK("Joystick Arcade Control via Right Stick"),
        JOYSTICK_ARCADE_LEFT_STICK("Joystick Arcade Control via Left Stick"),
        GAMEPAD_TANK("Gamepad Tank Control"),
        GAMEPAD_ARCADE_RIGHT_STICK("Gamepad Arcade Control via Right Stick"),
        GAMEPAD_ARCADE_LEFT_STICK("Gamepad Arcade Control via Left Stick");

        private final String description;

        ControlMode(String description) { this.description = description; }

        public String getDescription() { return description; }


        @Override
        public String toString() { return description; }
    }


    /**
     * This is a private constructor to ensure a single instance (i.e. singleton pattern).
     * Use the static {@link #getInstance()} method to obtain a reference to the subsystem.
     * For example, instead of doing this:
     * <pre>
     *     DriveTrainSubsystem subsystem = new DriveTrainSubsystem();
     * </pre>
     * do this:
     * <pre>
     *     DriveTrainSubsystem subsystem = DriveTrainSubsystem.getInstance();
     * </pre>
     */
    private DriveTrainSubsystem()
    {
        if (isEnabled())
        {
            try
            {
                logger.debug("Initializing {}", getClass().getSimpleName());
                logger.info("DISTANCE_PER_PULSE = '{}'", DISTANCE_PER_PULSE);
                initializeRobotDrive();
                initializeEncoders();
                logger.debug("{} initialization completed successfully", getClass().getSimpleName());
            }
            catch (Exception e)
            {
                Enablings.isDriveTrainSubsystemEnabled = false;
                logger.error("An exception occurred in {} constructor", getClass().getSimpleName(), e);
            }
        }
        else
        {
            logger.warn("{} is disabled and will not be initialized", getClass().getSimpleName());
        }
    }


    private void initializeRobotDrive()
    {
        logger.debug("Initializing RobotDrive");
        rightMotorController = new Victor(PWMs.RIGHT_DRIVE_TRAIN_MOTOR);
        rightMotorOps = new MotorOps(rightMotorController, false);

        leftMotorController = new Victor(PWMs.LEFT_DRIVE_TRAIN_MOTOR);
        leftMotorOps = new MotorOps(leftMotorController, false);

        robotDrive = new RobotDrive(leftMotorOps.getSpeedController(), rightMotorOps.getSpeedController());
        robotDrive.setSafetyEnabled(true);
        robotDrive.setExpiration(10);
        logger.debug("RobotDrive initialization completed");
    }


    public void setExpiration(double timeout)
    {
        robotDrive.setExpiration(timeout);
    }

    private void initializeEncoders()
    {
        logger.debug("Initializing DriveTrain Encoders");
        //noinspection PointlessBooleanExpression,ConstantConditions
        if ((DIOs.LEFT_DRIVE_TRAIN__MOTOR_ENCODER_CH_A >= 0) && (DIOs.LEFT_DRIVE_TRAIN__MOTOR_ENCODER_CH_B >= 0))
        {
            leftEncoderChADI = new DigitalInput(DIOs.LEFT_DRIVE_TRAIN__MOTOR_ENCODER_CH_A);
            leftEncoderChBDI = new DigitalInput(DIOs.LEFT_DRIVE_TRAIN__MOTOR_ENCODER_CH_B);
            leftEncoder = new Encoder(leftEncoderChADI,
                                      leftEncoderChBDI,
                                      true);

            // encoder.setDistancePerPulse((3.0 * Math.PI) / PULSES_PER_REVOLUTION);
            // distancePerPulse = (this.wheelDiameterInInches * Math.PI) / encoderPulsesPerRevolution;
            //encoder.setDistancePerPulse(distancePerPulse);
            leftEncoder.setDistancePerPulse(DISTANCE_PER_PULSE);

        }
        else
        {
            logger.warn("DIOs for Left Drive Encoder not set. (Ch A = {}; Ch B = {}). Left Encoder will not be Initialized!",
                        DIOs.LEFT_DRIVE_TRAIN__MOTOR_ENCODER_CH_A,
                        DIOs.LEFT_DRIVE_TRAIN__MOTOR_ENCODER_CH_B);
        }
        //noinspection PointlessBooleanExpression,ConstantConditions
        if ((DIOs.RIGHT_DRIVE_TRAIN__MOTOR_ENCODER_CH_A >= 0) && (DIOs.RIGHT_DRIVE_TRAIN__MOTOR_ENCODER_CH_B >= 0))
        {
            rightEncoderChADI = new DigitalInput(DIOs.RIGHT_DRIVE_TRAIN__MOTOR_ENCODER_CH_A);
            rightEncoderChBDI = new DigitalInput(DIOs.RIGHT_DRIVE_TRAIN__MOTOR_ENCODER_CH_B);
            rightEncoder = new Encoder(rightEncoderChADI,
                                       rightEncoderChBDI,
                                       false);
            rightEncoder.setDistancePerPulse(DISTANCE_PER_PULSE);
        }
        else
        {
            logger.warn("DIOs for Right Drive Encoder not set. (Ch A = {}; Ch B = {}). Right Encoder will not be Initialized!",
                        DIOs.RIGHT_DRIVE_TRAIN__MOTOR_ENCODER_CH_A,
                        DIOs.RIGHT_DRIVE_TRAIN__MOTOR_ENCODER_CH_B);
        }
        logger.debug("Encoder initialization completed");
    }


    public void setControlMode(ControlMode controlMode)
    {
        this.controlMode = controlMode;
    }


    public ControlMode getControlMode()
    {
        return controlMode;
    }


    @Override
    public void initDefaultCommand()
    {
        setDefaultCommand(new DriveControlModeSelectionCommand());
    }


    public static boolean isEnabled()
    {
        return Enablings.isDriveTrainSubsystemEnabled;
    }


    public void setDriveSensitivityCurvePower(int driveSensitivityCurvePower)
    {
        if (driveSensitivityCurvePower <= 0)
        {
            logger.warn("Attempt to set 'driveSensitivityCurvePower' to {}. Must be greater than zero. Defaulting to 1.", driveSensitivityCurvePower);
            driveSensitivityCurvePower = 1;
        }
        else if (driveSensitivityCurvePower > 10)
        {
            logger.warn("Attempt to set 'driveSensitivityCurvePower' to {}. Must be between 1 and 10 (inclusive). Limiting to 10.", driveSensitivityCurvePower);
            driveSensitivityCurvePower = 10;
        }
        this.driveSensitivityCurvePower = driveSensitivityCurvePower;
        updateDriveSensitivityCurvePowerReadout();
    }


    public void incrementDriveSensitivityCurvePower()
    {
        if (this.driveSensitivityCurvePower < 10)
        {
            this.driveSensitivityCurvePower++;
        }
        updateDriveSensitivityCurvePowerReadout();
    }


    public void decrementDriveSensitivityCurvePower()
    {
        if (this.driveSensitivityCurvePower > 1)
        {
            this.driveSensitivityCurvePower--;
        }
        updateDriveSensitivityCurvePowerReadout();
    }


    public void disableMotorSafetyForAutonomousMode()
    {
        robotDrive.setSafetyEnabled(false);
    }


    public void enableMotorSafetyPostAutonomousMode()
    {
        robotDrive.setSafetyEnabled(true);
    }


    public void stop()
    {
        robotDrive.stopMotor();
    }

    public void drive()
    {
        try
        {
            if (isEnabled())
            {
                switch (controlMode)
                {
                    case GAMEPAD_TANK:
                    {
                        final Joystick gamepad = Joysticks.GAMEPAD.getJoystick();
                        final double leftValue = gamepad.getRawAxis(Axises.LEFT_Y.getRawAxis());
                        final double rightValue = gamepad.getRawAxis(Axises.RIGHT_Y.getRawAxis());
                        driveViaTankDrive(leftValue, rightValue);
                    }

                        break;
                    case JOYSTICK_TANK:
//                        driveViaTankDrive(Joysticks.DRIVER_LEFT.getJoystick(), Joysticks.DRIVER_RIGHT.getJoystick());
                        break;
                    case GAMEPAD_ARCADE_LEFT_STICK:
                    {
                        final Joystick gamepad = Joysticks.GAMEPAD.getJoystick();
                        final double xValue = gamepad.getRawAxis(Axises.LEFT_X.getRawAxis());
                        final double yValue = gamepad.getRawAxis(Axises.LEFT_Y.getRawAxis());
                        driveViaArcadeDrive(yValue, xValue);
                    }
                    break;
                    case GAMEPAD_ARCADE_RIGHT_STICK:
                    {
                        final Joystick gamepad = Joysticks.GAMEPAD.getJoystick();
                        final double xValue = gamepad.getRawAxis(Axises.RIGHT_X.getRawAxis());
                        final double yValue = gamepad.getRawAxis(Axises.RIGHT_Y.getRawAxis());
                        driveViaArcadeDrive(yValue, xValue);
                    }
                    break;
                    case JOYSTICK_ARCADE_LEFT_STICK:
//                        driveViaArcadeDrive(Joysticks.DRIVER_LEFT.getJoystick());
                        break;
                    case JOYSTICK_ARCADE_RIGHT_STICK:
//                        driveViaArcadeDrive(Joysticks.DRIVER_RIGHT.getJoystick());
                        break;
                    default:
                        logger.error("Undefined ControlMode of {}", controlMode);
                }
            }
        }
        catch (Exception e)
        {
            logger.error("An exception occurred in DriveTrainSubsystem.drive()", e);
        }
    }


    public void drive(Direction direction, double speed)
    {
        speed = Math.abs(speed);
        switch (direction)
        {
            case FORWARD:
                driveStraightForward(speed);
                break;
            case REVERSE:
            case LEFT:
            case RIGHT:
            default:
             throw new UnsupportedOperationException("direction " + direction + " is not yet implemented");
        }

        sdbPutDistances();
    }

    public void driveViaArcadeDrive(GenericHID stick)
    {
        driveViaArcadeDrive(stick, true);
    }

    public void driveViaArcadeDrive(GenericHID stick, boolean applySensitivityCurve)
    {
        try
        {
            if ((stick == null))
            {
                logger.error("The joystick is null in DriveTrainSubsystem.driveViaArcadeDrive()");
            }
            else
            {
                driveViaArcadeDrive(stick.getY(), stick.getX(), applySensitivityCurve, STALL_SPEED);
            }
        }
        catch (Exception e)
        {
            logger.error("An exception occurred in DriveTrainSubsystem.driveViaArcadeDrive()", e);
        }
    }


    public void driveViaArcadeDrive(double yMoveValue, double xRotationValue)
    {
        driveViaArcadeDrive(yMoveValue, xRotationValue, true, STALL_SPEED);
    }

    public void driveViaArcadeDrive(double yMoveValue, double xRotationValue, boolean applySensitivityCurve, Double altStallSpeedToUse)
    {
        try
        {
            if (isEnabled())
            {

                final double stallSpeed = (altStallSpeedToUse != null) ? altStallSpeedToUse : STALL_SPEED;
                xRotationValue = MathUtils.scaleP1toN1SpeedToNonStallRange(xRotationValue, stallSpeed, JOYSTICK_DRIFT_CUT_OFF_VALUE);
                yMoveValue = MathUtils.scaleP1toN1SpeedToNonStallRange(yMoveValue, stallSpeed, JOYSTICK_DRIFT_CUT_OFF_VALUE);


                if (applySensitivityCurve)
                {
                    xRotationValue = adjustForSensitivityCurve(xRotationValue);
                    yMoveValue = adjustForSensitivityCurve(yMoveValue);
                }

                robotDrive.arcadeDrive(yMoveValue, xRotationValue);
            }
        }
        catch (Exception e)
        {
            logger.error("An exception occurred in DriveTrainSubsystem.driveViaArcadeDrive()", e);
        }
    }


    /**
     * Provide tank steering using the stored robot configuration. drive the robot using
     * two joystick inputs. The Y-axis will be selected from each Joystick object.
     *
     * @param leftStick  The joystick to control the left side of the robot.
     * @param rightStick The joystick to control the right side of the robot.
     */
    public void driveViaTankDrive(GenericHID leftStick, GenericHID rightStick)
    {
        driveViaTankDrive(leftStick, rightStick, true);
    }

    /**
     * Provide tank steering using the stored robot configuration. drive the robot using
     * two joystick inputs. The Y-axis will be selected from each Joystick object.
     *
     * @param leftStick  The joystick to control the left side of the robot.
     * @param rightStick The joystick to control the right side of the robot.
     */
    public void driveViaTankDrive(GenericHID leftStick, GenericHID rightStick, boolean applySensitivityCurve)
    {
        try
        {
            if ((leftStick == null) || (rightStick == null))
            {
                logger.error("A joystick is null in DriveTrainSubsystem.driveViaTankDrive()");
            }
            else
            {
                driveViaTankDrive(leftStick.getY(), rightStick.getY(), applySensitivityCurve);
            }
        }
        catch (Exception e)
        {
            logger.error("An exception occurred in DriveTrainSubsystem.driveViaTankDrive()", e);
        }
    }


    /**
     * Provide tank steering using the stored robot configuration. drive the robot using
     * two joystick inputs.
     *
     * @param leftValue  The value to set on the motor on the left side of the robot.
     * @param rightValue The value to set on the motor on the right side of the robot.
     */
    public void driveViaTankDrive(double leftValue, double rightValue)
    {
        driveViaTankDrive(leftValue, rightValue, true);
    }


    /**
     * Provide tank steering using the stored robot configuration. drive the robot using
     * two joystick inputs.
     *
     * @param leftValue  The value to set on the motor on the left side of the robot.
     * @param rightValue The value to set on the motor on the right side of the robot.
     */
    public void driveViaTankDrive(double leftValue, double rightValue, boolean applySensitivityCurve)
    {
        try
        {
            if (isEnabled())
            {
                logger.trace("driveViaTankDrive called with values leftValue = {}; rightValue = {}", leftValue, rightValue);
                leftValue = MathUtils.scaleP1toN1SpeedToNonStallRange(leftValue, STALL_SPEED, JOYSTICK_DRIFT_CUT_OFF_VALUE);
                rightValue = MathUtils.scaleP1toN1SpeedToNonStallRange(rightValue, STALL_SPEED, JOYSTICK_DRIFT_CUT_OFF_VALUE);

                if (applySensitivityCurve)
                {
                    leftValue = adjustForSensitivityCurve(leftValue);
                    rightValue = adjustForSensitivityCurve(rightValue);
                }

                logger.trace("calling robotDrive.tankDrive with adjusted values leftValue = {}; rightValue = {}", leftValue, rightValue);
                robotDrive.tankDrive(leftValue, rightValue);
//                    updateDistanceReadout();
            }
        }
        catch (Exception e)
        {
            logger.error("An exception occurred in DriveTrainSubsystem.driveViaTankDrive()", e);
        }
    }



    protected double adjustForSensitivityCurve(double v)
    {
        final boolean wasNegative = MathUtils.isNegative(v);
        final double curveValue = Math.pow(Math.abs(v), driveSensitivityCurvePower);
        return wasNegative ? -curveValue : curveValue;
    }


    protected void updateDriveSensitivityCurvePowerReadout()
    {
        logger.info("driveSensitivityCurvePower set to {}", this.driveSensitivityCurvePower);
    }

    protected void updateEncoderReadout()
    {
        final ImmutablePair<Integer, Integer> values = getEncoderValues();
        logger.debug("DriveTrain Encoders: left: {}\tright: {}", values.getLeft(), values.getRight());
    }

    public void resetEncoders()
    {
        if (leftEncoder != null)
        {
            leftEncoder.reset();
        }
        if (rightEncoder != null)
        {
            rightEncoder.reset();
        }
    }


    @Nullable
    public Encoder getRightEncoder()
    {
        return rightEncoder;
    }


    @Nullable
    public Encoder getLeftEncoder()
    {
        return leftEncoder;
    }


    public double getAverageDistanceInInches()
    {
        if ((leftEncoder != null) && (rightEncoder != null))
        {
            double total = leftEncoder.getDistance() +
                           rightEncoder.getDistance();

            return total / 2;
        }
        else
        {
            return Double.NaN;
        }
    }


    public void sdbPutDistances()
    {
        sdbPutAverageDistance();
        sdbPutSideDistances();
    }

    public void sdbPutSideDistances()
    {
        //noinspection PointlessBooleanExpression
        if (LogbackProgrammaticConfiguration.IN_DEBUGGING_MODE && (leftEncoder != null) && (rightEncoder != null))
        {
            SmartDashboard.putNumber(SDBKeys.LEFT_SIDE_DISTANCE, leftEncoder.getDistance());
            SmartDashboard.putNumber(SDBKeys.RIGHT_SIDE_DISTANCE, rightEncoder.getDistance());
        }
    }



    public void sdbPutAverageDistance()
    {
        if (LogbackProgrammaticConfiguration.IN_DEBUGGING_MODE)
        {
            final double avgDistance = getAverageDistanceInInches();
            //logger.trace("Average Distance: {}", avgDistance);
            SmartDashboard.putNumber(SDBKeys.AVG_DISTANCE, avgDistance);
        }
    }




    public void driveStraightForward(double absoluteSpeed)
    {
        if (isEnabled())
        {
            double speed = -Math.abs(absoluteSpeed);
            final ImmutablePair<Double, Double> speeds = adjustForPull(speed);
            driveViaTankDrive(speeds.getLeft(), speeds.getRight(), false);
        }
    }


    /**
     * Returns the encoder left and right values as a Pair. In the event an encoder
     * was not correctly initialized, {@link Integer#MIN_VALUE} is returned.
     * @return the encoder left and right values as a Pair
     */
    public ImmutablePair<Integer, Integer> getEncoderValues()
    {
        final int leftValue = (leftEncoder == null) ? Integer.MIN_VALUE : leftEncoder.get();
        final int rightValue = (rightEncoder == null) ? Integer.MIN_VALUE : rightEncoder.get();
        return new ImmutablePair<>(leftValue, rightValue);
    }

    public ImmutablePair<Double, Double> getSpeeds()
    {
        return new ImmutablePair<>(leftMotorController.get(), rightMotorController.get());
    }

    public ImmutablePair<Integer, Integer> getRawSpeeds()
    {
        return new ImmutablePair<>(leftMotorController.getRaw(), rightMotorController.getRaw());
    }


    public RobotDrive getRobotDrive()
    {
        return robotDrive;
    }



    public static enum Direction
    {
        FORWARD,
        REVERSE,
        LEFT,
        RIGHT
    }

    public static enum RotationDirection
    {
        CLOCKWISE
            {
                @Override
                public String toString()
                {
                    return "CW";
                }
            },
        COUNTERCLOCKWISE
            {
                @Override
                public String toString()
                {
                    return "CCW";
                }
            }
    }

    public static ImmutablePair<Double, Double> adjustForPull(double targetSpeed)
    {
        targetSpeed = MathUtils.constrainBetweenOneAndNegOne(targetSpeed);
        return new ImmutablePair<>(targetSpeed * PULL_FACTOR, targetSpeed);
    }

    /*******************************************************************************************************/
    /*  NO NON-SINGLETON INITIALIZATION CODE BELOW THIS POINT                                              */
    /*******************************************************************************************************/


    /**
     * *** THE SINGLETON FIELD NEEDS TO BE LAST FIELD DEFINED ****<br />
     * So we put it at the end of the class to be sure
     * All static fields/properties need to be initialized prior to the constructor running
     * If the singleton field (or a static init block) is put in the middle of the field declarations
     * (which can happen when the IDE automatically creates new Fields at the end of the list) subtle
     * and very hard to track down bugs can be introduced.
     */
    private static final DriveTrainSubsystem singleton = new DriveTrainSubsystem();


    /**
     * Gets the single instance of this subsystem. Use of this method replaces the use
     * of a constructor such as <tt>new DriveTrainSubsystem()</tt> in order to use ensure
     * only a single instance of a Subsystem is created. This is known as a Singleton.
     * For example, instead of doing this:
     * <pre>
     *     DriveTrainSubsystem subsystem = new DriveTrainSubsystem();
     * </pre>
     * do this:
     * <pre>
     *     DriveTrainSubsystem subsystem = DriveTrainSubsystem.getInstance();
     * </pre>
     *
     * @return the single instance of this subsystem.
     */
    public static DriveTrainSubsystem getInstance() {return singleton;}

    /*******************************************************************************************************/
    /*  NO CODE BELOW THIS POINT                                                                           */
    /*******************************************************************************************************/
}
