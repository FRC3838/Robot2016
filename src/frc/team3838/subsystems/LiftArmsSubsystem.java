package frc.team3838.subsystems;

//  written by Dario
//  created February 2, 2016
//
//  extend full
//  up
//  down
//  stop
//  tuck in
//  level
//  setup

import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.wpi.first.wpilibj.Encoder;
import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.Relay;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.command.Subsystem;
import frc.team3838.config.DIOs;
import frc.team3838.config.Enablings;
import frc.team3838.config.Relays;



/**
 * Arms Class for RocCityRobotix robot2016
 *
 * Public Methods Are:
 * <code> public Arms() </code>             // Calls init() method.
 * <code> public void init() </code>        // initializes
 *
 *
 * <code> public void in() </code>          // moves Arms in fast until MIN limit then pulses in.
 * <code> public void out() </code>         // moves Arms out fast until MAX limit then pulses out.
 * <code> public void pulseIn() </code>     // move Arms in slowly using pulses
 * <code> public void pulseOut() </code>    // moves Arms out slowly using pulses.
 *
 * <code> public int getLeftEncoder() </code>   // returns Left Encoder Reading
 * <code> public int getRightEncoder() </code>  // returns Right Encoder Reading
 *
 * <code> public boolean setupLeft() </code>    // pulses Left Arm In, Resets Encoder, returns true if encoder >= starting value.
 * <code> public boolean setupRight() </code>   // pulses Right Arm In, Resets Encoder, Returns true if encoder >= starting value.
 *
 *
 * The following methods move fast to Loose Limits, the Pulse to Tight Limits.  All return true when within Tight Limits.
 * <code> public boolean extend() </code>    // Moves Arms to the Floor.
 * <code> public boolean retract() </code>   // Retracts Arms into the Robot.
 * <code> public boolean level() </code>     // Moves Arms to Level Position to Lift the Sally Port.
 * <code> public boolean climb() </code>     // Moves Arms out to climbing position to help keep Robot from tipping.
 *
 * @author Dario Kulic and Kurt M. Sanger
 */
@SuppressWarnings({"BooleanMethodNameMustStartWithQuestion", "IfMayBeConditional", "unused", "SimplifiableIfStatement"})
public class LiftArmsSubsystem extends Subsystem
{

    private static final Logger logger = LoggerFactory.getLogger(LiftArmsSubsystem.class);

    public static final long PERIODIC_EXECUTE_INTERVAL_DURATION = 5;
    public static final TimeUnit PERIODIC_EXECUTE_INTERVAL_TIME_UNIT = TimeUnit.MILLISECONDS;

    static final long PULSE_TIME = 1000000L;
    //static final double PULSE_ON_TIME = 0.025; //PULSE_ON_TIME in seconds
    //static final double PULSE_OFF_TIME = 0.25; //wait time between pulses in seconds
    static final double PULSE_ON_TIME = 0.012; //PULSE_ON_TIME in seconds
    static final double PULSE_OFF_TIME = 0.012; //wait time between pulses in seconds
    // Wait time shorter than loop time (100 ms ?) effectively does nothing.
    static final int MAXIMUM_TRIES = 12;

    static final int OUT = 0;  // used for next move.
    static final int STOP = 1; // used for next move.
    static final int IN = 2;   // used for next move.

    static final int FAST_MAX_LIMIT = 200;
    static final int FAST_MIN_LIMIT = 15;
    
    static final int STICK_MOVE_AMOUNT = 5;

    private enum RetractLimits
    {
        LOOSE__MIN(-5),
        LOOSE_MAX(15),
        TIGHT_MIN(-15),
        TIGHT_MAX(1);
        private final int value;


        RetractLimits(final int newValue)
        {
            value = newValue;
        }


        public int getValue() { return value; }
    }

    private enum LevelLimits
    {
        LOOSE_MIN(140),
        LOOSE_MAX(175),
        TIGHT_MIN(150),
        TIGHT_MAX(165);
        private final int value;


        LevelLimits(final int newValue)
        {
            value = newValue;
        }


        public int getValue() { return value; }
    }

    private enum ExtendLimits
    {
        LOOSE_MIN(200),
        LOOSE_MAX(225),
        TIGHT_MIN(220),
        TIGHT_MAX(225);
        private final int value;


        ExtendLimits(final int newValue)
        {
            value = newValue;
        }


        public int getValue() { return value; }
    }

    private enum ClimbLimits
    {
        LOOSE_MIN(95),
        LOOSE_MAX(125),
        TIGHT_MIN(105),
        TIGHT_MAX(115);
        private final int value;


        ClimbLimits(final int newValue)
        {
            value = newValue;
        }


        public int getValue() { return value; }
    }


    Relay leftSpike;
    Relay rightSpike;
    Encoder leftEncoder;
    Encoder rightEncoder;
    Timer pulseTimer;
    Timer leftOffTimer;
    Timer rightOffTimer;


    int leftPosition;
    int rightPosition;
	boolean leftMotorOnFlg;
	boolean rightMotorOnFlg;


    public LiftArmsSubsystem()
    {
        if (isEnabled())
        {
            try
            {
                logger.debug("Initializing {}", getClass().getSimpleName());

                init();

                logger.debug("{} initialization completed successfully", getClass().getSimpleName());
            }
            catch (Exception e)
            {
                Enablings.isArmsSubsystemEnabled = false;
                logger.error("An exception occurred in {} constructor", getClass().getSimpleName(), e);
            }
        }
        else
        {
            logger.warn("{} is disabled and will not be initialized", getClass().getSimpleName());
        }
    }


    public void init()
    {
        leftSpike = new Relay(Relays.LEFT_ARM_SPIKE, Relay.Direction.kBoth);
        rightSpike = new Relay(Relays.RIGHT_ARM_SPIKE, Relay.Direction.kBoth);
        leftSpike.set(Relay.Value.kOff);
        rightSpike.set(Relay.Value.kOff);
		leftMotorOnFlg = false;
		rightMotorOnFlg = false;

        leftEncoder = new Encoder(DIOs.LEFT_ARM_ENCODER_CH_A, DIOs.LEFT_ARM_ENCODER_CH_B);
        rightEncoder = new Encoder(DIOs.RIGHT_ARM_ENCODER_CH_A, DIOs.RIGHT_ARM_ENCODER_CH_B);
        leftEncoder.reset();
        rightEncoder.reset();

        pulseTimer = new Timer();
        pulseTimer.start();
        leftOffTimer = new Timer();
        rightOffTimer = new Timer();
        leftOffTimer.start();
        rightOffTimer.start();

    }


    private void leftStopWithoutTimerReset()
    {
        leftSpike.set(Relay.Value.kOff);
		leftMotorOnFlg = false;
    }


    private boolean pulseLeftIn()
    {
        return pulseSideIn(leftSpike, leftOffTimer);
    }

    private void leftIn()
    {
        if (leftEncoder.get() > FAST_MIN_LIMIT)
        {
            leftSpike.set(Relay.Value.kReverse);
			leftMotorOnFlg = true;
        }
        else
        {
            leftStopWithoutTimerReset();  // turn off in case motor is on.
            // move in slowly
            pulseLeftIn();
        }
    }


    private void rightStopWithoutTimerReset()
    {
        rightSpike.set(Relay.Value.kOff);
		rightMotorOnFlg = false;
    }


    private boolean pulseRightIn()
    {
        return pulseSideIn(rightSpike, rightOffTimer);
    }


    private void rightIn()
    {
        if (rightEncoder.get() > FAST_MIN_LIMIT)
        {
            rightSpike.set(Relay.Value.kReverse);
			rightMotorOnFlg = true;
        }
        else
        {
            rightStopWithoutTimerReset(); // stop in case motor is running.
            pulseRightIn();
        }
    }


    private static boolean pulseSideIn(Relay spikeRelay, Timer offTimer)
    {
        if (offTimer.get() > PULSE_OFF_TIME)
        {
            spikeRelay.set(Relay.Value.kReverse);
            Timer.delay(PULSE_ON_TIME);
            spikeRelay.set(Relay.Value.kOff);
            offTimer.reset();
            return true;
        }
        else
        {
            return false;
        }
    }

    /**
     * <code> public void in() </code>
     * Moves both arms in.
     * will test for FAST_MIN_LIMIT limit.
     * If < MIN limit then it pulses Arms in slowly.
     */
    public void in()
    {
        leftIn();
        rightIn();
        //leftSpike.set(Relay.Value.kReverse);
        //rightSpike.set(Relay.Value.kReverse);
    }


    /**
     * <code> public void pulseIn() </code>
     * Pulses both arms in slowly.
     */
    public void pulseIn()
    {
        // stop motors in case they were on
        leftStopWithoutTimerReset();
        rightStopWithoutTimerReset();
        pulseLeftIn();
        pulseRightIn();
    }


    private void pulseLeftOut()
    {
        pulseSideOut(leftSpike, leftOffTimer);
    }


    private void leftOut()
    {
        if (leftEncoder.get() < FAST_MAX_LIMIT)
        {
            leftSpike.set(Relay.Value.kForward);
			leftMotorOnFlg = true;
        }
        else
        {
            leftStopWithoutTimerReset();  // turn off motor in case its running.
            pulseLeftOut();
        }
    }


    private void pulseRightOut()
    {
        pulseSideOut(rightSpike, rightOffTimer);
    }


    private void rightOut()
    {
        if (rightEncoder.get() < FAST_MAX_LIMIT)
        {
            rightSpike.set(Relay.Value.kForward);
			rightMotorOnFlg = true;
        }
        else
        {
            rightStopWithoutTimerReset();  // turn off motor in case its running.
            pulseRightOut();
        }
    }


    /**
     * <code> public void out() </code>
     * moves both arms out.
     * Tests for FAST_MAX_LIMIT and will
     * pulse out slowly if > limit.
     */
    public void out()
    {
        leftOut();
        rightOut();
    }

    private static void pulseSideOut(Relay spikeRelay, Timer offTimer)
    {
        if (offTimer.get() > PULSE_OFF_TIME)
        {
            spikeRelay.set(Relay.Value.kForward);
            Timer.delay(PULSE_ON_TIME);
            spikeRelay.set(Relay.Value.kOff);
            offTimer.reset();
        }
    }


    /**
     * <code> public void pulseOut() </code>
     * pulses both arms out slowly.
     */
    public void pulseOut()
    {
        // stop motors in case they were running.
        leftStopWithoutTimerReset();
        rightStopWithoutTimerReset();
        pulseLeftOut();
        pulseRightOut();
    }


    private void leftStop()
    {
        leftSpike.set(Relay.Value.kOff);
		if (leftMotorOnFlg) {
			leftOffTimer.reset();			
			leftMotorOnFlg = false;
		}
    }


    private void rightStop()
    {
        rightSpike.set(Relay.Value.kOff);
		if (rightMotorOnFlg) {
			rightOffTimer.reset();
			rightMotorOnFlg = false;
		}
    }


    /**
     * <code> public void stop() </code>
     * Stops both arms.
     * Resets left and right off timers for pulsing.
     */
    public void stop()
    {
        leftStop();
        rightStop();
    }


    /**
     * <code> public boolean setupLeft() </code>
     * Pulses Left Arm In Slowly.
     * Resets Left Arm Encoder to Zero.
     * Returns true if encoder reading did not get smaller.
     */
    public boolean setupLeft()
    {
        return setupSide(leftEncoder, leftSpike, leftOffTimer);
    }


    /**
     * <code> public boolean setupRight() </code>
     * Pulses Right Arm In Slowly.
     * Resets Right Arm Encoder to Zero.
     * Returns true if encoder reading did not get smaller.
     */
    public boolean setupRight()
    {
        return setupSide(rightEncoder, rightSpike, rightOffTimer);
    }


    /**
     * Pulses Arm In Slowly.
     * Resets Arm Encoder to Zero.
     * Returns true if encoder reading did not get smaller.
     */
    public static boolean setupSide(Encoder encoder, Relay spikeRelay, Timer offTimer)
    {
        int startPosition;
        int currentPosition;

        startPosition = encoder.get();
        if (pulseSideIn(spikeRelay, offTimer))
        {
            currentPosition = encoder.get();

            if (currentPosition < startPosition)
            {
                encoder.reset();
                return false;
            }
            else
            {
                encoder.reset();
                return true;
            }
        }
        else
        {
            // we did not pulse need more delay
            encoder.reset();
            return false;
        }
    }

    /**
     * <code> public int getLeftEncoder() </code>
     * Returns Encoder Value for Left Arm
     *
     * @return Encoder Value for Left Arm
     */
    public int getLeftEncoder()
    {
        return leftEncoder.get();
    }


    /**
     * <code> public int getRightEncoder() </code>
     * Returns Encoder Value for Right Arm
     *
     * @return Encoder Value for Right Arm
     *
     */
    public int getRightEncoder()
    {
        return rightEncoder.get();
    }


    private boolean moveToPosition(int minEncoderValue, int maxEncoderValue)
    {
        int currentLeftPos;
        int currentRightPos;
        @SuppressWarnings("UnusedAssignment")
        int nextLeftMove = STOP;
        @SuppressWarnings("UnusedAssignment")
        int nextRightMove = STOP;
        int currentState;
        boolean doneFlg = false;

        // read left and right encoders
        currentLeftPos = leftEncoder.get();
        currentRightPos = rightEncoder.get();

        // test left encoder to determine next Left Move.
        if (currentLeftPos < minEncoderValue)
        {
            nextLeftMove = OUT;
        }
        else
        {
            if (currentLeftPos > maxEncoderValue)
            {
                nextLeftMove = IN;
            }
            else
            {
                nextLeftMove = STOP;
            }
        }

        // test right encoder to determine next Right Move.
        if (currentRightPos < minEncoderValue)
        {
            nextRightMove = OUT;
        }
        else
        {
            if (currentRightPos > maxEncoderValue)
            {
                nextRightMove = IN;
            }
            else
            {
                nextRightMove = STOP;
            }
        }

        // Compute armState = 3*left + right;
        currentState = (3 * nextLeftMove) + nextRightMove;

        // Switch on armState
        switch (currentState)
        {
            case 0:   // Both Left and Right Move OUT
                out();
                break;
            case 1:   // Left moves out.  Right is done.
                leftOut();
                rightStop();
                break;
            case 2:  // Left moves out.  Right moves in.
                leftOut();
                rightIn();
                break;
            case 3:  // Left is done, Right moves Out
                leftStop();
                rightOut();
                break;
            case 4:  // Left is done, Right is done.  We are done.
                stop();
                doneFlg = true;
                break;
            case 5:  // Left is done, Right moves In.
                leftStop();
                rightIn();
                break;
            case 6:  // Left moves In, Right moves Out.
                leftIn();
                rightOut();
                break;
            case 7: // Left moves In, Right is done.
                leftIn();
                rightStop();
                break;
            case 8: // Left moves In, Right moves In.
                in();
                break;
            default: // We have an error.  Stop.
                stop();
                logger.error("Arm state > 8");
                break;
        }
        return doneFlg;
    }


    private boolean pulseToPosition(int minEncoderValue, int maxEncoderValue)
    {
        int currentLeftPos;
        int currentRightPos;
        @SuppressWarnings("UnusedAssignment")
        int nextLeftMove = STOP;
        @SuppressWarnings("UnusedAssignment")
        int nextRightMove = STOP;
        int currentState;
        boolean doneFlg = false;

        // read left and right encoders
        currentLeftPos = leftEncoder.get();
        currentRightPos = rightEncoder.get();

        // test left encoder to determine next Left Move.
        if (currentLeftPos < minEncoderValue)
        {
            nextLeftMove = OUT;
        }
        else
        {
            if (currentLeftPos > maxEncoderValue)
            {
                nextLeftMove = IN;
            }
            else
            {
                nextLeftMove = STOP;
            }
        }

        // test right encoder to determine next Right Move.
        if (currentRightPos < minEncoderValue)
        {
            nextRightMove = OUT;
        }
        else
        {
            if (currentRightPos > maxEncoderValue)
            {
                nextRightMove = IN;
            }
            else
            {
                nextRightMove = STOP;
            }
        }

        // Compute armState = 3*left + right;
        currentState = (3 * nextLeftMove) + nextRightMove;

        // Switch on armState
        switch (currentState)
        {
            case 0:   // Both Left and Right Move OUT
                pulseLeftOut();
                pulseRightOut();
                break;
            case 1:   // Left moves out.  Right is done.
                pulseLeftOut();
                rightStop();
                break;
            case 2:  // Left moves out.  Right moves in.
                pulseLeftOut();
                pulseRightIn();
                break;
            case 3:  // Left is done, Right moves Out
                leftStop();
                pulseRightOut();
                break;
            case 4:  // Left is done, Right is done.  We are done.
                stop();
                doneFlg = true;
                break;
            case 5:  // Left is done, Right moves In.
                leftStop();
                pulseRightIn();
                break;
            case 6:  // Left moves In, Right moves Out.
                pulseLeftIn();
                pulseRightOut();
                break;
            case 7: // Left moves In, Right is done.
                pulseLeftIn();
                rightStop();
                break;
            case 8: // Left moves In, Right moves In.
                pulseLeftIn();
                pulseRightIn();
                break;
            default: // We have an error.  Stop.
                stop();
                logger.error("Arm State > 8");
                break;
        }
        return doneFlg;
    }


    /**
     * <code> public boolean extend() </code>
     * Extends arms to floor using ExtendLimits.
     * Uses Fast Moves to Loose Limits.
     * Pulses to Tight Limits
     * Returns true if within ExtendLimits TIGHT_MIN and TIGHT_MAX values.
     *
     * @return true if within ExtendLimits TIGHT_MIN and TIGHT_MAX values.
     */
    public boolean extend()
    {
        if (moveToPosition(ExtendLimits.LOOSE_MIN.getValue(), ExtendLimits.LOOSE_MAX.getValue()))
        {
            return pulseToPosition(ExtendLimits.TIGHT_MIN.getValue(), ExtendLimits.TIGHT_MAX.getValue());
        }
        else
        {
            return false;
        }
    }


    /**
     * <code> public boolean retract() </code>
     * Retracts arms into Robot using RetractLimits.
     * Uses Fast Moves to Loose Limits.
     * Pulses to Tight Limits
     * Returns true if within RetractLimits TIGHT_MIN and TIGHT_MAX values.
     *
     * @return true if within RetractLimits TIGHT_MIN and TIGHT_MAX values.
     */
    public boolean retract()
    {
        if (moveToPosition(RetractLimits.LOOSE__MIN.getValue(), RetractLimits.LOOSE_MAX.getValue()))
        {
            return pulseToPosition(RetractLimits.TIGHT_MIN.getValue(), RetractLimits.TIGHT_MAX.getValue());
        }
        else
        {
            return false;
        }
    }


    /**
     * <code> public boolean level() </code>
     * Moves arms to level position using LevelLimits.
     * Uses Fast Moves to Loose Limits.
     * Pulses to Tight Limits
     * Returns true if within LevelLimits TIGHT_MIN and TIGHT_MAX values.
     *
     * @return true if within LevelLimits TIGHT_MIN and TIGHT_MAX values
     */
    public boolean level()
    {
        if (moveToPosition(LevelLimits.LOOSE_MIN.getValue(), LevelLimits.LOOSE_MAX.getValue()))
        {
            return pulseToPosition(LevelLimits.TIGHT_MIN.getValue(), LevelLimits.TIGHT_MAX.getValue());
        }
        else
        {
            return false;
        }
    }


    /**
     * <code> public boolean climb() </code>
     * Moves Arms to climb position using ClimbLimits.
     * Uses Fast Moves to Loose Limits.
     * Pulses to Tight Limits
     * Returns true if within ClimbLimits TIGHT_MIN and TIGHT_MAX values.
     *
     * @return true if within ClimbLimits TIGHT_MIN and TIGHT_MAX values.
     *
     */
    public boolean climb()
    {
        if (moveToPosition(ClimbLimits.LOOSE_MIN.getValue(), ClimbLimits.LOOSE_MAX.getValue()))
        {
            return pulseToPosition(ClimbLimits.TIGHT_MIN.getValue(), ClimbLimits.TIGHT_MAX.getValue());
        }
        else
        {
            return false;
        }
    }
    
    
    private void moveWithStick(double stickY) {
    	// Function to move using the joystick.
    	int leftPos = getLeftEncoder();
    	
		if (stickY < -0.25) {
			// We want to move out.
			leftPos += STICK_MOVE_AMOUNT;
			if (leftPos > ExtendLimits.TIGHT_MAX.getValue()) {
				leftPos = ExtendLimits.TIGHT_MIN.getValue();
			}
			pulseToPosition(leftPos,leftPos+5);
		} else {
			if (stickY > 0.25) {
				// We want to move in.
				leftPos -= STICK_MOVE_AMOUNT;
				if (leftPos < RetractLimits.TIGHT_MAX.getValue()) {
					leftPos = RetractLimits.TIGHT_MAX.getValue();
				}
				pulseToPosition(leftPos,leftPos-5);
			} else {
				stop();
			}
		}
    }


    @Override
    public void initDefaultCommand()
    {
        // TODO: Set the default command for the subsystem here.
        //setDefaultCommand(new MyCommand());
    }


    /**
     * Method that can be called periodically via a scheduler to execute methods
     * based on the received Joysticks' state. This method will be periodically
     * called bu a thread executor in {@link frc.team3838.Robot3838} class
     * <em>when in teleop mode.</em> The frequency of calling is set via the
     * {@link LiftArmsSubsystem#PERIODIC_EXECUTE_INTERVAL_DURATION} and
     * {@link #PERIODIC_EXECUTE_INTERVAL_TIME_UNIT} constants in this class,
     * current set to {@value #PERIODIC_EXECUTE_INTERVAL_DURATION}
     * {@value #PERIODIC_EXECUTE_INTERVAL_TIME_UNIT}.
     *
     * @param joystick the joystick to monitor.
     */
    public void periodicExecute(Joystick joystick)
    {
        try
        {
            logger.trace("{}.periodicExecute(Joystick) called", getName());
            //TODO: Kurt: add your code here
            
            int stickButtons;
            stickButtons = 0;
            
        	// Read all the buttons on the stick.
        	if (joystick.getRawButton(1)) {
        		// Trigger Button used for manual in/out slow/fast
        		stickButtons += 1;
        	}
        	if (joystick.getRawButton(2)) {
        		// Bottom Top Button for Retract
        		stickButtons += 2;
        	}
        	if (joystick.getRawButton(3)) {
        		// Middle Top Button for Extend to Floor
        		stickButtons += 4;
        	}
        	if (joystick.getRawButton(4)) {
        		// Left Top Button for Level / Lift
        		stickButtons += 8;
        	}
        	if (joystick.getRawButton(5)) {
        		// Right Top Button for Climb
        		stickButtons += 16;
        	}
        	//if (stick.getRawButton(6)) {
        		// UNUSED
        		//stickButtons += 32;
        	//}
        	//if (joystick.getRawButton(7)) {
        	    // UNUSED
        		//stickButtons += 64;
        	//}
        	if (joystick.getRawButton(8)) {
        		// Bottom Left Button for Setup Left Arm
        		stickButtons += 128;
        	}
        	if (joystick.getRawButton(9)) {
        		// Bottom Right Button for Setup Right Arm
        		stickButtons += 256;
        	}
        	//if (joystick.getRawButton(10)) {
        		// UNUSED
        	    //stickButtons += 512;
        	//}
        	//if (joystick.getRawButton(11)) {
    		    // UNUSED
        		//stickButtons += 1024;
        	//}
       	
        	// Test which button combinations are pressed.
        	switch (stickButtons) {
        		case 1 :
        			// Trigger Button 1 Read Y Axis
        			double stickY;
        			moveWithStick(joystick.getY());
        			break;
        		case 2 :
        			// Bottom Top Button 2
        			// Retract
        			retract();
        			break;
        		case 4 :
        			// Top Top Button 3
        			// Extend to Floor
        			extend();
        			break;
        		case 8 :
        			// Top Left Button 4
        			// Go To Level
        			level();
        			break;
        		case 16 :
        			// Top Right Button 5
        			// Go To Climb Position
        			climb();
        			break;
        		//case 32 :
        			// Button 6
        			// Arms Out
        			//out();
        			// break;
        		//case 64 :
        			// Button 7
        			// Arms In
        			// in();
        			// break;
        		case 128 :
        			// Button 8
        			// Setup Left Arm
        			if ( setupLeft() ) {
        				// Left encoder stopped moving in.
        			} else {
        				// Left Encoder Moved In.
        				// or Motor did not pulse.
        			}
        			break;
        		case 256 :
        			// Button 9
        			// Setup Right Arm
        			if ( setupRight() ) {
        				// Right Encoder Stopped Moving In.
        				
        			} else {
        				// Right Encoder Moved In or
        				// Motor did not pulse.
        			}
        			break;
        		//case 512 :
        			// Button 10
        			// Pulse In
        			// pulseIn();
        			// break;
        		//case 1024 :
        			// Button 11
        			// Pulse Out
        			// pulseOut();
        			// break;
        		default :
        			// No Buttons or Multiple Buttons
        			stop();
        			break;
        	}

        }
        catch (Throwable e)
        {
            //noinspection UnnecessaryToStringCall
            logger.error("{}.periodicExecute() threw an exception. Cause Summary: {}", getName(), e.toString(), e);
        }
    }


    public static boolean isEnabled()
    {
        return Enablings.isArmsSubsystemEnabled;
    }


    /*******************************************************************************************************/
    /*  NO NON-SINGLETON INITIALIZATION CODE BELOW THIS POINT                                              */
    /*******************************************************************************************************/


    /**
     * *** THE SINGLETON FILED NEEDS TO BE LAST FIELD DEFINED ****<br />
     * So we put it at the end of the class to be sure
     * All static fields/properties need to be initialized prior to the constructor running
     * If the singleton field (or a static init block) is put in the middle of the field declarations
     * (which can happen when the IDE automatically creates new Fields at the end of the list) subtle
     * and very hard to track down bugs can be introduced.
     */
    private static final LiftArmsSubsystem singleton = new LiftArmsSubsystem();


    /**
     * Gets the single instance of this subsystem. Use of this method replaces the use
     * of a constructor such as <tt>new LiftArmsSubsystem()</tt> in order to use ensure
     * only a single instance of a Subsystem is created. This is known as a Singleton.
     * For example, instead of doing this:
     * <pre>
     *     LiftArmsSubsystem subsystem = new LiftArmsSubsystem();
     * </pre>
     * do this:
     * <pre>
     *     LiftArmsSubsystem subsystem = LiftArmsSubsystem.getInstance();
     * </pre>
     *
     * @return the single instance of this subsystem.
     */
    public static LiftArmsSubsystem getInstance() {return singleton;}

    /*******************************************************************************************************/
    /*  NO CODE BELOW THIS POINT                                                                           */
    /*******************************************************************************************************/
}