package frc.team3838.subsystems;


import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import edu.wpi.first.wpilibj.CANTalon;
import edu.wpi.first.wpilibj.CANTalon.FeedbackDevice;
import edu.wpi.first.wpilibj.SpeedController;
import edu.wpi.first.wpilibj.Talon;
import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc.team3838.config.CANs;
import frc.team3838.config.Enablings;
import frc.team3838.config.PWMs;
import frc.team3838.utils.MathUtils;
import frc.team3838.utils.logging.LogbackProgrammaticConfiguration;
import frc.team3838.utils.motors.MotorOps;



public class BallomaticSubsystem extends Subsystem
{
    @SuppressWarnings("UnusedDeclaration")
    private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(BallomaticSubsystem.class);

    private CANTalon ballArmTalonSrx;
    private MotorOps ballArmMotorOps;

    private MotorOps ballFeedMotorOps;

    private boolean isBrakeModeOn = true;

    private double armManualMoveSpeed = 0.7;

    private final ScheduledExecutorService scheduledExecutorService =  Executors.newScheduledThreadPool(1);



    private double armSpeed = 0.50;
    private double armDownSlowSpeed = 0.1;
    private double armUpSlowSpeed = 0.27;

    private State state = State.STOPPED;

    private boolean inManualMode = false;
    private static final int ZERO_POINT_OFFSET = 517;


    public enum EncoderLimits
    {
        MID_POINT(0.060, 0),
        NEARING_MID_POINT(0.170, 150),
        NEARING_PICK_POSITION(0.210, 750),
        PICK_POSITION(0.250, 900);

        private final double positionValue;
        private final int encPositionValue;


        EncoderLimits(double positionValue, int encPositionValue)
        {
            this.positionValue = positionValue;
            this.encPositionValue = encPositionValue;
        }


        public double getPositionValue()
        {
            return positionValue;
        }


        public int getEncPositionValue()
        {
            return encPositionValue;
        }
    }


    enum State
    {
        MOVING_UP,
        MOVING_DOWN,
        AT_TARGET,
        STOPPED
    }

    /**
     * This is a private constructor to ensure a single instance (i.e. singleton pattern).
     * Use the static {@link #getInstance()} method to obtain a reference to the subsystem.
     * For example, instead of doing this:
     * <pre>
     *     BallomaticSubsystem subsystem = new BallomaticSubsystem();
     * </pre>
     * do this:
     * <pre>
     *     BallomaticSubsystem subsystem = BallomaticSubsystem.getInstance();
     * </pre>
     */
    private BallomaticSubsystem()
    {
        if (isEnabled())
        {
            try
            {
                logger.debug("Initializing {}", getClass().getSimpleName());

                scheduledExecutorService.scheduleAtFixedRate(new UpdateStateRunnable(), 0, 1, TimeUnit.MILLISECONDS);

                ballArmTalonSrx = new CANTalon(CANs.BALL_ARM_MOTOR);
                isBrakeModeOn = true;
                //noinspection ConstantConditions
                ballArmTalonSrx.enableBrakeMode(isBrakeModeOn);
                ballArmMotorOps = new MotorOps(ballArmTalonSrx, false);
                ballArmTalonSrx.changeControlMode(CANTalon.TalonControlMode.PercentVbus);



                //From §7.5.4 pg 49
                //      Absolute Mode Update rate (period) is 400 ms and uses Pulse Width API
                //      Relative Mode Update rate (period) is 100 us and uses Quadrature API
                //      The advantage of absolute mode is having a solid reference to where a mechanism is
                //          without re-tare-ing or re-zero-ing the robot
                //      The advantage of the relative mode is the faster update rate.
                //      However both values can be read/written at the same time.


                // From §13.1
                //      setPosition() can be used to change the current sensor position, if a relative sensor is used.
                //          talonSrx.setPosition(0); /* set the sensor position to zero */


                // Examples from §5.3
                //      int quadEncoderPosition = talonSrx.getEncPosition();






                logValues("After Construction with default FeedbackDevice");
                ballArmTalonSrx.setFeedbackDevice(FeedbackDevice.CtreMagEncoder_Absolute);
                try {TimeUnit.MILLISECONDS.sleep(50);} catch (InterruptedException ignore) {}
                logValues("After setting Feedback Device to CtreMagEncoder_Absolute");
                ballArmTalonSrx.setFeedbackDevice(FeedbackDevice.CtreMagEncoder_Relative);
                try {TimeUnit.MILLISECONDS.sleep(50);} catch (InterruptedException ignore) {}
                logValues("Initial Values after setting Feedback Device to CtreMagEncoder_Relative");

                final int zeroPoint = ballArmTalonSrx.getPulseWidthPosition() - ZERO_POINT_OFFSET;

                //Seems like this needs to be called a couple of timing to take. Might be timing issue?
                for (int i = 0; i < 5; i++)
                {
                    ballArmTalonSrx.setEncPosition(zeroPoint);
                    try {TimeUnit.MILLISECONDS.sleep(50);} catch (InterruptedException ignore) {}
                }
                logValues("After setting Encoder to ZeroPoint (in CtreMagEncoder_Relative)");





                //TODO: Need to verify if this is a Victor or talon
                final SpeedController ballIntakeSpeedController = new Talon(PWMs.BALL_INTAKE_MOTOR);
                ballFeedMotorOps = new MotorOps(ballIntakeSpeedController, false);

                logger.debug("{} initialization completed successfully", getClass().getSimpleName());
            }
            catch (Exception e)
            {
                Enablings.isBallSubsystemEnabled = false;
                logger.error("An exception occurred in {} constructor", getClass().getSimpleName(), e);
            }
        }
        else
        {
            logger.warn("{} is disabled and will not be initialized", getClass().getSimpleName());
        }
    }




    @Override
    public void initDefaultCommand()
    {
//        setDefaultCommand(new BallomaticReadCommand());
    }


    @SuppressWarnings("Duplicates")
    public void logValues(String headerMsg)
    {
        logger.debug(headerMsg);
        logger.debug("\t\tBOM talon.getPosition(): {}", ballArmTalonSrx.getPosition());
        logger.debug("\t\tBOM talon.getAnalogInPosition(): {}", ballArmTalonSrx.getAnalogInPosition());
        logger.debug("\t\tBOM talon.getAnalogInRaw(): {}", ballArmTalonSrx.getAnalogInRaw());
        logger.debug("\t\tBOM talon.getAnalogInVelocity(): {}", ballArmTalonSrx.getAnalogInVelocity());
        logger.debug("\t\tBOM talon.getControlMode(): {}", ballArmTalonSrx.getControlMode());
        logger.debug("\t\tBOM talon.getEncPosition(): {}", ballArmTalonSrx.getEncPosition());
        logger.debug("\t\tBOM talon.getEncPosition() scaled to 360: {}", getArmAngle());
        logger.debug("\t\tBOM talon.getEncVelocity(): {}", ballArmTalonSrx.getEncVelocity());
        logger.debug("\t\tBOM talon.getPulseWidthPosition(): {}", ballArmTalonSrx.getPulseWidthPosition());
        logger.debug("\t\tBOM talon.getPulseWidthPosition() MASKED: {}", ballArmTalonSrx.getPulseWidthPosition() & 0xFFF);
        logger.debug("\t\tBOM talon.getSetpoint(): {}", ballArmTalonSrx.getSetpoint());
        logger.debug("\t\tBOM talon.pidGet(): {}", ballArmTalonSrx.pidGet());
    }


    @SuppressWarnings("Duplicates")
    public void reportValues()
    {
        if (LogbackProgrammaticConfiguration.IN_DEBUGGING_MODE)
        {
            SmartDashboard.putNumber("BOM talon.getPosition():          ", ballArmTalonSrx.getPosition());
            SmartDashboard.putNumber("BOM talon.getAnalogInPosition():  ", ballArmTalonSrx.getAnalogInPosition());
            SmartDashboard.putNumber("BOM talon.getAnalogInRaw():       ", ballArmTalonSrx.getAnalogInRaw());
            SmartDashboard.putNumber("BOM talon.getAnalogInVelocity():  ", ballArmTalonSrx.getAnalogInVelocity());
            SmartDashboard.putString("BOM talon.getControlMode():       ", ballArmTalonSrx.getControlMode().name());
            SmartDashboard.putNumber("BOM talon.getEncPosition():       ", ballArmTalonSrx.getEncPosition());
            SmartDashboard.putNumber("BOM talon.getEncPosition() scaled to 360: ", getArmAngle());
            SmartDashboard.putNumber("BOM talon.getEncVelocity():       ", ballArmTalonSrx.getEncVelocity());
            SmartDashboard.putNumber("BOM talon.getPulseWidthPosition():", ballArmTalonSrx.getPulseWidthPosition());
            SmartDashboard.putNumber("BOM talon.getPulseWidthPosition() MASKED: ", ballArmTalonSrx.getPulseWidthPosition() & 0xFFF);
            SmartDashboard.putNumber("BOM talon.getSetpoint():          ", ballArmTalonSrx.getSetpoint());
            SmartDashboard.putNumber("BOM talon.pidGet():               ", ballArmTalonSrx.pidGet());
        }
    }


    public double getArmAngle() {return MathUtils.scaleRange(ballArmTalonSrx.getEncPosition(), 0, 4096, 0, 360);}


    public void moveToUpPosition()
    {
        if (isEnabled() && (ballArmMotorOps != null))
        {
            if (isArmAllTheWayUp() || (state == State.AT_TARGET))
            {
                ballArmMotorOps.stop();
                state = State.STOPPED;
            }
            else
            {
                state = State.MOVING_UP;
                double speed = isArmNearUpPosition() ? armUpSlowSpeed : armSpeed;
                ballArmMotorOps.setReverseSpeed(speed);
            }
        }
    }

    public void moveToPickPosition()
    {
        if (isEnabled() && (ballArmMotorOps != null))
        {
            if (isArmAllTheWayDown() || (state == State.AT_TARGET))
            {
                ballArmMotorOps.stop();
                state = State.STOPPED;
            }
            else
            {
                state = State.MOVING_DOWN;
                double speed = isArmNearPickPosition() ? armDownSlowSpeed : armSpeed;
                ballArmMotorOps.setForwardSpeed(speed);
            }
        }
    }


    public class UpdateStateRunnable implements Runnable
    {
        @Override
        public void run()
        {
            switch (state)
            {
                case MOVING_DOWN:
                    if (isArmAllTheWayDown())
                    {
                        ballArmMotorOps.stop();
                        state = State.AT_TARGET;
                    }
                    break;
                case MOVING_UP:
                    if (isArmAllTheWayUp())
                    {
                        ballArmMotorOps.stop();
                        state = State.AT_TARGET;
                    }
                    break;
            }
        }
    }


    public boolean isArmAllTheWayDown()
    {
        return !isEnabled() || ((getArmPosition() > EncoderLimits.PICK_POSITION.getPositionValue()) || (state == State.AT_TARGET));
    }


    public boolean isArmNearPickPosition()
    {
        return !isEnabled() || (getArmPosition() > EncoderLimits.NEARING_PICK_POSITION.getPositionValue());
    }


    public boolean isArmAllTheWayUp()
    {
        return !isEnabled() || ((getArmPosition() < EncoderLimits.MID_POINT.getPositionValue()) || (state == State.AT_TARGET));
    }


    public boolean isArmNearUpPosition()
    {
        return !isEnabled() || (getArmPosition() < EncoderLimits.NEARING_MID_POINT.getPositionValue());
    }


    public double getArmPosition()
    {
        return (isEnabled() && (ballArmTalonSrx != null)) ? ballArmTalonSrx.getPosition() : Double.NaN;
    }


    public void manualMoveArmUp()
    {
        if (isEnabled() && inManualMode)
        {
            try
            {
                ballArmMotorOps.setReverseSpeed(armManualMoveSpeed);
            }
            catch (Exception e)
            {
                logger.debug("Exception when moving ball arm up. Cause Summary: {}", e.toString());
            }
        }
    }

    public void manualMoveArmDown()
    {
        if (isEnabled() && inManualMode)
        {
            try
            {

                ballArmMotorOps.setForwardSpeed(armManualMoveSpeed);
            }
            catch (Exception e)
            {
                logger.debug("Exception when moving ball arm down. Cause Summary: {}", e.toString());
            }
        }
    }

    public void stopArm()
    {
        try
        {
            ballArmTalonSrx.set(0);
        }
        catch (Exception e)
        {
            logger.debug("Exception when stopping arm motor. Cause Summary: {}", e.toString());
        }
    }


    public void turnFeedMotorOnForIntake()
    {
        if (isEnabled() && (ballFeedMotorOps != null))
        {
            ballFeedMotorOps.setForwardSpeed(MathUtils.constrainBetweenZeroAndOne(Math.abs(readFeedMotorIntakeAbsoluteSpeed())));
        }
    }

    public void turnFeedMotorOnForDischarge()
    {
        if (isEnabled() && (ballFeedMotorOps != null))
        {
            ballFeedMotorOps.setReverseSpeed(MathUtils.constrainBetweenZeroAndOne(Math.abs(readFeedMotorDischargeAbsoluteSpeed())));
        }
    }

    public void stopFeedMotor()
    {
        try
        {
            ballFeedMotorOps.stop();
        }
        catch (Exception e)
        {
            logger.debug("Exception when stopping feed motor. Cause Summary: {}", e.toString());
        }
    }


    public void setInManualMode(boolean inManualMode)
    {
        this.inManualMode = inManualMode;
        logger.debug("inManualMode set to: '{}'", inManualMode);
    }


    private static double readFeedMotorIntakeAbsoluteSpeed()
    {
        return 0.5;
//        try
//        {
//            return MathUtils.constrainBetweenZeroAndOne(SmartDashboard.getNumber(FEED_INTAKE_SPEED_SDB_KEY, 0.50));
//        }
//        catch (Exception e)
//        {
//            logger.warn("Exception when reading {} from SmartDashboard. Cause Summary: {}",
//                        FEED_INTAKE_SPEED_SDB_KEY, e.toString());
//            return 0.5;
//        }
    }


    private static double readFeedMotorDischargeAbsoluteSpeed()
    {
        return 1;
//        try
//        {
//            return MathUtils.constrainBetweenZeroAndOne(SmartDashboard.getNumber(FEED_DISCHARGE_SPEED_SDB_KEY, 0.50));
//        }
//        catch (Exception e)
//        {
//            logger.warn("Exception when reading {} from SmartDashboard. Cause Summary: {}",
//                        FEED_DISCHARGE_SPEED_SDB_KEY, e.toString());
//            return 0.5;
//        }
    }



    public static boolean isEnabled()
    {
        return Enablings.isBallSubsystemEnabled;
    }


    /*******************************************************************************************************/
    /*  NO NON-SINGLETON INITIALIZATION CODE BELOW THIS POINT                                              */
    /*******************************************************************************************************/


    /**
     * *** THE SINGLETON FIELD NEEDS TO BE LAST FIELD DEFINED ****<br />
     * So we put it at the end of the class to be sure
     * All static fields/properties need to be initialized prior to the constructor running
     * If the singleton field (or a static init block) is put in the middle of the field declarations
     * (which can happen when the IDE automatically creates new Fields at the end of the list) subtle
     * and very hard to track down bugs can be introduced.
     */
    private static final BallomaticSubsystem singleton = new BallomaticSubsystem();


    /**
     * Gets the single instance of this subsystem. Use of this method replaces the use
     * of a constructor such as <tt>new BallomaticSubsystem()</tt> in order to use ensure
     * only a single instance of a Subsystem is created. This is known as a Singleton.
     * For example, instead of doing this:
     * <pre>
     *     BallomaticSubsystem subsystem = new BallomaticSubsystem();
     * </pre>
     * do this:
     * <pre>
     *     BallomaticSubsystem subsystem = BallomaticSubsystem.getInstance();
     * </pre>
     *
     * @return the single instance of this subsystem.
     */
    public static BallomaticSubsystem getInstance() {return singleton;}

    /*******************************************************************************************************/
    /*  NO CODE BELOW THIS POINT                                                                           */
    /*******************************************************************************************************/

}
