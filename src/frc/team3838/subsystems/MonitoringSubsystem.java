package frc.team3838.subsystems;


import edu.wpi.first.wpilibj.AnalogInput;
import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc.team3838.commands.monitor.MonitoringCommand;
import frc.team3838.config.AIOs;
import frc.team3838.config.Enablings;



public class MonitoringSubsystem extends Subsystem
{
    @SuppressWarnings("UnusedDeclaration")
    private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(MonitoringSubsystem.class);


    protected AnalogInput armMonitor1;
    protected AnalogInput armMonitor2;

    /**
     * This is a private constructor to ensure a single instance (i.e. singleton pattern).
     * Use the static {@link #getInstance()} method to obtain a reference to the subsystem.
     * For example, instead of doing this:
     * <pre>
     *     MonitoringSubsystem subsystem = new MonitoringSubsystem();
     * </pre>
     * do this:
     * <pre>
     *     MonitoringSubsystem subsystem = MonitoringSubsystem.getInstance();
     * </pre>
     */
    private MonitoringSubsystem()
    {
        if (isEnabled())
        {
            try
            {
                logger.debug("Initializing {}", getClass().getSimpleName());

                armMonitor2 = new AnalogInput(AIOs.ARM_MONITOR_2);
                armMonitor1 = new AnalogInput(AIOs.ARM_MONITOR_1);
                buttonReleased();


                logger.debug("{} initialization completed successfully", getClass().getSimpleName());
            }
            catch (Exception e)
            {
                Enablings.isMonitoringSubsystemEnabled = false;
                logger.error("An exception occurred in {} constructor", getClass().getSimpleName(), e);
            }
        }
        else
        {
            logger.warn("{} is disabled and will not be initialized", getClass().getSimpleName());
        }
    }


    public void monitorArms()
    {
        SmartDashboard.putData("Arm 1", armMonitor1);
        SmartDashboard.putData("Arm 2", armMonitor2);
    }

    @Override
    public void initDefaultCommand()
    {
        if (isEnabled())
        {
            setDefaultCommand(new MonitoringCommand());
        }
    }

    public void buttonPushed(String name)
    {
//        SmartDashboard.putString("Button", name);
    }

    public void buttonReleased()
    {
//        SmartDashboard.putString("Button", "- - - - - - - ");
    }


    public static boolean isEnabled()
    {
        return Enablings.isMonitoringSubsystemEnabled;
    }


    /*******************************************************************************************************/
    /*  NO NON-SINGLETON INITIALIZATION CODE BELOW THIS POINT                                              */
    /*******************************************************************************************************/


    /**
     * *** THE SINGLETON FILED NEEDS TO BE LAST FIELD DEFINED ****<br />
     * So we put it at the end of the class to be sure
     * All static fields/properties need to be initialized prior to the constructor running
     * If the singleton field (or a static init block) is put in the middle of the field declarations
     * (which can happen when the IDE automatically creates new Fields at the end of the list) subtle
     * and very hard to track down bugs can be introduced.
     */
    private static final MonitoringSubsystem singleton = new MonitoringSubsystem();


    /**
     * Gets the single instance of this subsystem. Use of this method replaces the use
     * of a constructor such as <tt>new MonitoringSubsystem()</tt> in order to use ensure
     * only a single instance of a Subsystem is created. This is known as a Singleton.
     * For example, instead of doing this:
     * <pre>
     *     MonitoringSubsystem subsystem = new MonitoringSubsystem();
     * </pre>
     * do this:
     * <pre>
     *     MonitoringSubsystem subsystem = MonitoringSubsystem.getInstance();
     * </pre>
     *
     * @return the single instance of this subsystem.
     */
    public static MonitoringSubsystem getInstance() {return singleton;}

    /*******************************************************************************************************/
    /*  NO CODE BELOW THIS POINT                                                                           */
    /*******************************************************************************************************/
}
