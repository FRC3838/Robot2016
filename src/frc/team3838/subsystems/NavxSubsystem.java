package frc.team3838.subsystems;


import java.util.concurrent.TimeUnit;
import javax.annotation.Nullable;

import com.kauailabs.navx.frc.AHRS;

import edu.wpi.first.wpilibj.DriverStation.Alliance;
import edu.wpi.first.wpilibj.SPI;
import edu.wpi.first.wpilibj.command.Subsystem;
import frc.team3838.commands.navx.NavxTestingCommand;
import frc.team3838.config.Enablings;
import frc.team3838.utils.MathUtils;



public class NavxSubsystem extends Subsystem
{

    /*
        ***IMPORTANT NOTE:
            Due to a board layout issue on the navX-MXP, there is noticeable
            crosstalk between ANALOG IN pins 3, 2 and 1. For that reason,
            use of pin 3 and pin 2 is NOT RECOMMENDED.***
     */


    @SuppressWarnings("UnusedDeclaration")
    private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(NavxSubsystem.class);

    @Nullable
    private AHRS navx;
    private Alliance alliance;

    private final double startingPitch;



    /**
     * This is a private constructor to ensure a single instance (i.e. singleton pattern).
     * Use the static {@link #getInstance()} method to obtain a reference to the subsystem.
     * For example, instead of doing this:
     * <pre>
     *     NavxSubsystem subsystem = new NavxSubsystem();
     * </pre>
     * do this:
     * <pre>
     *     NavxSubsystem subsystem = NavxSubsystem.getInstance();
     * </pre>
     */
    private NavxSubsystem()
    {
        double tempStartingPitch = 0;
        if (isEnabled())
        {
            try
            {
                logger.debug("Initializing {}", getClass().getSimpleName());
                final long startTime = System.currentTimeMillis();
                navx = new AHRS(SPI.Port.kMXP);
                logger.info("Waiting for NAVx to calibrate.");
                while (navx.isCalibrating())
                {
                    final int timeOutInMs = 2_000;
                    if ((System.currentTimeMillis() - startTime) > timeOutInMs)
                    {
                        logger.warn("NAVx calibration did not complete within {}ms. "
                                    + "This likely means the NAVx board is not connected. "
                                    + "The {} will be DISABLED.", timeOutInMs, getClass().getSimpleName());
                        disableNavxSubsystem();
                        break;
                    }
                    logger.info("NAVx is calibrating...");
                    try {TimeUnit.MILLISECONDS.sleep(250);} catch (InterruptedException ignore) {}
                }

                if (navx != null)
                {
                    navx.reset();
                    logger.info("NAVx calibration completed. Initialization took {}ms", System.currentTimeMillis() - startTime);
                    logger.info("NAVx Firmware Version: {}", navx.getFirmwareVersion());
                    tempStartingPitch = navx.getPitch();

                    final String headerMsg = "NAVx Initialization Values";
                    logNavxValues(headerMsg);


                    logger.debug("{} initialization completed successfully", getClass().getSimpleName());
                }
            }
            catch (Throwable e)
            {
                disableNavxSubsystem();
                logger.error("An exception occurred in {} constructor. The Subsystem will be DISABLED.", getClass().getSimpleName(), e);
            }
        }
        else
        {

            logger.warn("{} is disabled and will not be initialized", getClass().getSimpleName());
        }
        startingPitch = tempStartingPitch;
    }


    public void logNavxValues(String headerMsg)
    {
        if (navx != null)
        {
            logger.info(headerMsg);
            logger.info("\tNAVx altitude: {}", MathUtils.formatNumber(navx.getAltitude(), 4));
            logger.info("\tNAVx angle: {}", MathUtils.formatNumber(navx.getAngle(), 4));
            logger.info("\tNAVx CompassHeading: {}", MathUtils.formatNumber(navx.getCompassHeading(), 4));
            logger.info("\tNAVx Fused Heading: {}", MathUtils.formatNumber(navx.getFusedHeading(), 4));
            logger.info("\tNAVx Rate: {}", MathUtils.formatNumber(navx.getRate(), 4));
            logger.info("\tNAVx Pitch: {}", MathUtils.formatNumber(navx.getPitch(), 4));
            logger.info("\tNAVx Roll: {}", MathUtils.formatNumber(navx.getRoll(), 4));
            logger.info("\tNAVx Yaw: {}", MathUtils.formatNumber(navx.getYaw(), 4));
            logger.info("\tNAVx DisplacementX: {}", MathUtils.formatNumber(navx.getDisplacementX(), 4));
            logger.info("\tNAVx DisplacementY: {}", MathUtils.formatNumber(navx.getDisplacementY(), 4));
            logger.info("\tNAVx DisplacementZ: {}", MathUtils.formatNumber(navx.getDisplacementZ(), 4));
        }
    }

    public void logDisplacement(String statusMsg)
    {
        if (navx != null)
        {
            logger.info("Displacement (in meters) @ {}: x:{}; y={}; z={}", statusMsg, navx.getDisplacementX(), navx.getDisplacementY(), navx.getDisplacementZ());
        }
    }

    private void disableNavxSubsystem()
    {
        Enablings.isNavxSubsystemEnabled = false;
        if (navx != null)
        {
            navx.free();
            navx = null;
        }
    }


    public double getStartingPitch()
    {
        return startingPitch;
    }


    @Override
    public void initDefaultCommand()
    {
        setDefaultCommand(new NavxTestingCommand());
    }

    public static boolean isEnabled()
    {
        return Enablings.isNavxSubsystemEnabled;
    }


    @Nullable
    public AHRS getNavx()
    {
        return navx;
    }


    /*******************************************************************************************************/
    /*  NO NON-SINGLETON INITIALIZATION CODE BELOW THIS POINT                                              */
    /*******************************************************************************************************/


    /**
     * *** THE SINGLETON FILED NEEDS TO BE LAST FIELD DEFINED ****<br />
     * So we put it at the end of the class to be sure
     * All static fields/properties need to be initialized prior to the constructor running
     * If the singleton field (or a static init block) is put in the middle of the field declarations
     * (which can happen when the IDE automatically creates new Fields at the end of the list) subtle
     * and very hard to track down bugs can be introduced.
     */
    private static final NavxSubsystem singleton = new NavxSubsystem();


    /**
     * Gets the single instance of this subsystem. Use of this method replaces the use
     * of a constructor such as <tt>new NavxSubsystem()</tt> in order to use ensure
     * only a single instance of a Subsystem is created. This is known as a Singleton.
     * For example, instead of doing this:
     * <pre>
     *     NavxSubsystem subsystem = new NavxSubsystem();
     * </pre>
     * do this:
     * <pre>
     *     NavxSubsystem subsystem = NavxSubsystem.getInstance();
     * </pre>
     *
     * @return the single instance of this subsystem.
     */
    public static NavxSubsystem getInstance() {return singleton;}

    /*******************************************************************************************************/
    /*  NO CODE BELOW THIS POINT                                                                           */
    /*******************************************************************************************************/
}
