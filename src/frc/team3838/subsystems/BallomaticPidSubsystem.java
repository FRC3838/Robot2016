package frc.team3838.subsystems;


import java.util.concurrent.TimeUnit;

import edu.wpi.first.wpilibj.CANTalon;
import edu.wpi.first.wpilibj.CANTalon.FeedbackDevice;
import edu.wpi.first.wpilibj.CANTalon.TalonControlMode;
import edu.wpi.first.wpilibj.SpeedController;
import edu.wpi.first.wpilibj.Talon;
import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc.team3838.config.CANs;
import frc.team3838.config.Enablings;
import frc.team3838.config.PWMs;
import frc.team3838.utils.MathUtils;
import frc.team3838.utils.motors.MotorOps;



public class BallomaticPidSubsystem extends Subsystem
{
    @SuppressWarnings("UnusedDeclaration")
    private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(BallomaticPidSubsystem.class);

    private CANTalon ballArmTalonSrx;
    private MotorOps ballArmMotorOps;

    private MotorOps ballFeedMotorOps;

    private boolean isBrakeModeOn = true;

    private double armManualMoveSpeed = 0.990;


    private boolean inManualMode = false;
    private static final int ZERO_POINT_OFFSET = 517;


    public enum ArmPosition
    {
        STRAIGHT_UP(0),
        MID_POINT(512),
        HIGH_PICK_POSITION(950),
        PICK_POSITION(1024);

        private final double setpoint;

        ArmPosition(double setpoint) {this.setpoint = setpoint;}

        public double getSetpoint()
        {
            return setpoint;
        }
    }


    /**
     * This is a private constructor to ensure a single instance (i.e. singleton pattern).
     * Use the static {@link #getInstance()} method to obtain a reference to the subsystem.
     * For example, instead of doing this:
     * <pre>
     *     BallomaticSubsystem subsystem = new BallomaticSubsystem();
     * </pre>
     * do this:
     * <pre>
     *     BallomaticSubsystem subsystem = BallomaticSubsystem.getInstance();
     * </pre>
     */
    private BallomaticPidSubsystem()
    {
        if (isEnabled())
        {
            try
            {
                logger.debug("Initializing {}", getClass().getSimpleName());

                ballArmTalonSrx = new CANTalon(CANs.BALL_ARM_MOTOR);
                isBrakeModeOn = true;
                //noinspection ConstantConditions
                ballArmTalonSrx.enableBrakeMode(isBrakeModeOn);
                ballArmMotorOps = new MotorOps(ballArmTalonSrx, false);




                //From §7.5.4 pg 49
                //      Absolute Mode Update rate (period) is 400 ms and uses Pulse Width API
                //      Relative Mode Update rate (period) is 100 us and uses Quadrature API
                //      The advantage of absolute mode is having a solid reference to where a mechanism is
                //          without re-tare-ing or re-zero-ing the robot
                //      The advantage of the relative mode is the faster update rate.
                //      However both values can be read/written at the same time.


                // From §13.1
                //      setPosition() can be used to change the current sensor position, if a relative sensor is used.
                //          talonSrx.setPosition(0); /* set the sensor position to zero */


                // Examples from §5.3
                //      int quadEncoderPosition = talonSrx.getEncPosition();


                ballArmTalonSrx.changeControlMode(TalonControlMode.PercentVbus);
                ballArmTalonSrx.enableControl();


                boolean IN_FORCED_MANUAL_MODE = readForceManualMode();
                logger.info("IN_FORCED_MANUAL_MODE = '{}'", IN_FORCED_MANUAL_MODE);
                if (!IN_FORCED_MANUAL_MODE)
                {
                    try {TimeUnit.MILLISECONDS.sleep(700);} catch (InterruptedException ignore) {}
                    logValues("After Construction with default FeedbackDevice");
                    ballArmTalonSrx.setFeedbackDevice(FeedbackDevice.CtreMagEncoder_Absolute);
                    try {TimeUnit.MILLISECONDS.sleep(700);} catch (InterruptedException ignore) {}
                    logValues("After setting Feedback Device to CtreMagEncoder_Absolute");
                    ballArmTalonSrx.setFeedbackDevice(FeedbackDevice.CtreMagEncoder_Relative);
                    logValues("Initial Values after setting Feedback Device to CtreMagEncoder_Relative");

                    final int zeroPoint = ballArmTalonSrx.getPulseWidthPosition() - ZERO_POINT_OFFSET;

                    //Seems like this needs to be called a couple of timing to take. Might be timing issue?
                    for (int i = 0; i < 5; i++)
                    {
                        ballArmTalonSrx.setEncPosition(zeroPoint);
                        ballArmTalonSrx.setPosition(zeroPoint);
                        ballArmTalonSrx.set(0);
                        try {TimeUnit.MILLISECONDS.sleep(50);} catch (InterruptedException ignore) {}
                    }
                    logValues("After setting Encoder to ZeroPoint (in CtreMagEncoder_Relative)");


//                /* choose the sensor and sensor direction */
                    ballArmTalonSrx.changeControlMode(TalonControlMode.Position);
                    ballArmTalonSrx.reverseSensor(false);

//                /* set the peak and nominal outputs, 12V means full */
                    ballArmTalonSrx.configNominalOutputVoltage(+0f, -0f);
                    ballArmTalonSrx.configPeakOutputVoltage(+6f, -6f);
//                /* set the allowable closed-loop error,
//                 * Closed-Loop output will be neutral within this range.
//                 * See Table in Section 17.2.1 for native units per rotation.
//                 */
                    ballArmTalonSrx.setAllowableClosedLoopErr(0); /* always servo */


                /* set closed loop gains in slot0 */
                    ballArmTalonSrx.setProfile(0);
                    ballArmTalonSrx.setF(0.0);
                    ballArmTalonSrx.setP(readP());
                    ballArmTalonSrx.setI(readI());
                    ballArmTalonSrx.setD(readD());


                    ballArmTalonSrx.setSetpoint(0);
                }


                //TODO: Need to verify if this is a Victor or talon
                final SpeedController ballIntakeSpeedController = new Talon(PWMs.BALL_INTAKE_MOTOR);
                ballFeedMotorOps = new MotorOps(ballIntakeSpeedController, false);

                logger.debug("{} initialization completed successfully", getClass().getSimpleName());
            }
            catch (Exception e)
            {
                Enablings.isBallPidSubsystemEnabled = false;
                logger.error("An exception occurred in {} constructor", getClass().getSimpleName(), e);
            }
        }
        else
        {
            logger.warn("{} is disabled and will not be initialized", getClass().getSimpleName());
        }
    }


    private static double readP()
    {
        return Double.parseDouble(SmartDashboard.getString("Ball2 PID P", MathUtils.formatNumber(0.1, 6)));
    }

    private static double readI()
    {
        return Double.parseDouble(SmartDashboard.getString("Ball2 PID I", MathUtils.formatNumber(0.004, 6)));
    }


    private static double readD()
    {
        return Double.parseDouble(SmartDashboard.getString("Ball2 PID D", MathUtils.formatNumber(0.0, 6)));
    }


    private void initializeArm()
    {
        final double position = ballArmTalonSrx.getPosition();
        // Up = 0.122
        // Down = ~0.3
    }

    @Override
    public void initDefaultCommand()
    {
//        setDefaultCommand(new BallomaticReadCommand());
    }


    public void logValues(String headerMsg)
    {
        logger.info(headerMsg);
        logger.info("\t\tBOM talon.getPosition(): {}", ballArmTalonSrx.getPosition());
        logger.info("\t\tBOM talon.getAnalogInPosition(): {}", ballArmTalonSrx.getAnalogInPosition());
        logger.info("\t\tBOM talon.getAnalogInRaw(): {}", ballArmTalonSrx.getAnalogInRaw());
        logger.info("\t\tBOM talon.getAnalogInVelocity(): {}", ballArmTalonSrx.getAnalogInVelocity());
        logger.info("\t\tBOM talon.getControlMode(): {}", ballArmTalonSrx.getControlMode());
        logger.info("\t\tBOM talon.getEncPosition(): {}", ballArmTalonSrx.getEncPosition());
        logger.info("\t\tBOM talon.getEncPosition() scaled to 360: {}", getArmAngle());
        logger.info("\t\tBOM talon.getEncVelocity(): {}", ballArmTalonSrx.getEncVelocity());
        logger.info("\t\tBOM talon.getPulseWidthPosition(): {}", ballArmTalonSrx.getPulseWidthPosition());
        logger.info("\t\tBOM talon.getPulseWidthPosition() MASKED: {}", ballArmTalonSrx.getPulseWidthPosition() & 0xFFF);
        logger.info("\t\tBOM talon.getSetpoint(): {}", ballArmTalonSrx.getSetpoint());
        logger.info("\t\tBOM talon.pidGet(): {}", ballArmTalonSrx.pidGet());
    }





    public void reportValues()
    {
        SmartDashboard.putNumber("BOM talon.getPosition():          ", ballArmTalonSrx.getPosition());
        SmartDashboard.putNumber("BOM talon.getAnalogInPosition():  ", ballArmTalonSrx.getAnalogInPosition());
        SmartDashboard.putNumber("BOM talon.getAnalogInRaw():       ", ballArmTalonSrx.getAnalogInRaw());
        SmartDashboard.putNumber("BOM talon.getAnalogInVelocity():  ", ballArmTalonSrx.getAnalogInVelocity());
        SmartDashboard.putString("BOM talon.getControlMode():       ", ballArmTalonSrx.getControlMode().name());
        SmartDashboard.putNumber("BOM talon.getEncPosition():       ", ballArmTalonSrx.getEncPosition());
        SmartDashboard.putNumber("BOM talon.getEncPosition() scaled to 360: ", getArmAngle());
        SmartDashboard.putNumber("BOM talon.getEncVelocity():       ", ballArmTalonSrx.getEncVelocity());
        SmartDashboard.putNumber("BOM talon.getPulseWidthPosition():", ballArmTalonSrx.getPulseWidthPosition());
        SmartDashboard.putNumber("BOM talon.getPulseWidthPosition() MASKED: ", ballArmTalonSrx.getPulseWidthPosition() & 0xFFF);
        SmartDashboard.putNumber("BOM talon.getSetpoint():          ", ballArmTalonSrx.getSetpoint());
        SmartDashboard.putNumber("BOM talon.pidGet():               ", ballArmTalonSrx.pidGet());
    }


    public double getArmAngle() {return MathUtils.scaleRange(ballArmTalonSrx.getEncPosition(), 0, 4096, 0, 360);}

    public double convertAngleToSetpoint(double angle)
    {
        return MathUtils.scaleRange(ballArmTalonSrx.getEncPosition(), 0, 360, 0, 4096);
    }

    public void gotoPosition(ArmPosition armPosition)
    {
        logger.debug("gotoPosition called with armPosition = '{}'", armPosition);
        SmartDashboard.putNumber("Target setpoint", armPosition.getSetpoint());
        ballArmTalonSrx.setSetpoint(armPosition.getSetpoint());
    }


    private static boolean readForceManualMode()
    {
        return SmartDashboard.getBoolean("Force Manual Mode", false);
    }

    public void updatePidValues()
    {
        ballArmTalonSrx.setP(readP());
        ballArmTalonSrx.setI(readI());
        ballArmTalonSrx.setD(readD());
//        IN_FORCED_MANUAL_MODE = readForceManualMode();

        ballArmTalonSrx.setSetpoint(SmartDashboard.getNumber("Target setpoint"));

    }

    static
    {
        SmartDashboard.putNumber("Target setpoint", 0);
        SmartDashboard.putBoolean("Force Manual Mode", false);
    }

//    static
//    {
//        SmartDashboard.putString("Ball2 PID P", MathUtils.formatNumber(1.0, 6));
//        SmartDashboard.putString("Ball2 PID I", MathUtils.formatNumber(0.004, 6));
//        SmartDashboard.putString("Ball2 PID D", MathUtils.formatNumber(0.0, 6));
//        SmartDashboard.putString("Pick Position", MathUtils.formatNumber(0.300, 6));
//    }


    public void manualMoveArmUp()
    {
        if (isEnabled() && inManualMode)
        {
            try
            {
                ballArmMotorOps.setReverseSpeed(armManualMoveSpeed);
                ballArmTalonSrx.setSetpoint(ballArmTalonSrx.pidGet());
            }
            catch (Exception e)
            {
                logger.debug("Exception when moving ball arm up. Cause Summary: {}", e.toString());
            }
        }
    }

    public void manualMoveArmDown()
    {
        if (isEnabled() && inManualMode)
        {
            try
            {
                ballArmMotorOps.setForwardSpeed(armManualMoveSpeed);
                ballArmTalonSrx.setSetpoint(ballArmTalonSrx.pidGet());
            }
            catch (Exception e)
            {
                logger.debug("Exception when moving ball arm down. Cause Summary: {}", e.toString());
            }
        }
    }

    public void updateSetPointToCurrent()
    {
        ballArmTalonSrx.setSetpoint(ballArmTalonSrx.get());
    }

    public void stopArm()
    {
        try
        {
            ballArmTalonSrx.set(0);
        }
        catch (Exception e)
        {
            logger.debug("Exception when stopping arm motor. Cause Summary: {}", e.toString());
        }
    }


    public void turnFeedMotorOnForIntake()
    {
        if (isEnabled() && (ballFeedMotorOps != null))
        {
            ballFeedMotorOps.setForwardSpeed(MathUtils.constrainBetweenZeroAndOne(Math.abs(readFeedMotorIntakeAbsoluteSpeed())));
        }
    }

    public void turnFeedMotorOnForDischarge()
    {
        if (isEnabled() && (ballFeedMotorOps != null))
        {
            ballFeedMotorOps.setReverseSpeed(MathUtils.constrainBetweenZeroAndOne(Math.abs(readFeedMotorDischargeAbsoluteSpeed())));
        }
    }

    public void stopFeedMotor()
    {
        try
        {
            ballFeedMotorOps.stop();
        }
        catch (Exception e)
        {
            logger.debug("Exception when stopping feed motor. Cause Summary: {}", e.toString());
        }
    }


    public void setInManualMode(boolean inManualMode)
    {
        this.inManualMode = inManualMode;
        if (inManualMode)
        {
            ballArmTalonSrx.changeControlMode(TalonControlMode.PercentVbus);
            //do we need to call ::  ballArmTalonSrx.disableControl();
        }
        else
        {
            ballArmTalonSrx.changeControlMode(TalonControlMode.Position);

        }
        logger.debug("inManualMode set to: '{}'", inManualMode);
    }


    private static double readFeedMotorIntakeAbsoluteSpeed()
    {
        return 0.5;
//        try
//        {
//            return MathUtils.constrainBetweenZeroAndOne(SmartDashboard.getNumber(FEED_INTAKE_SPEED_SDB_KEY, 0.50));
//        }
//        catch (Exception e)
//        {
//            logger.warn("Exception when reading {} from SmartDashboard. Cause Summary: {}",
//                        FEED_INTAKE_SPEED_SDB_KEY, e.toString());
//            return 0.5;
//        }
    }


    private static double readFeedMotorDischargeAbsoluteSpeed()
    {
        return 1;
//        try
//        {
//            return MathUtils.constrainBetweenZeroAndOne(SmartDashboard.getNumber(FEED_DISCHARGE_SPEED_SDB_KEY, 0.50));
//        }
//        catch (Exception e)
//        {
//            logger.warn("Exception when reading {} from SmartDashboard. Cause Summary: {}",
//                        FEED_DISCHARGE_SPEED_SDB_KEY, e.toString());
//            return 0.5;
//        }
    }


    public double getArmPosition()
    {
        return (isEnabled() && (ballArmTalonSrx != null)) ? ballArmTalonSrx.getPosition() : -1;
    }


    public void setEncoderPosition(int newPosition)
    {
        logger.info("Setting Encoder Position to {}", newPosition);
        ballArmTalonSrx.setEncPosition(newPosition);
    }


    public boolean toggleBrakeMode()
    {
        isBrakeModeOn = !isBrakeModeOn;
        logger.debug("isBrakeModeOn = '{}'", isBrakeModeOn);
        return isBrakeModeOn;
    }


    public void setBrakeMode(boolean brakeMode)
    {
        isBrakeModeOn = brakeMode;
        logger.debug("isBrakeModeOn = '{}'", isBrakeModeOn);
    }


    public static boolean isEnabled()
    {
        return Enablings.isBallPidSubsystemEnabled;
    }


    /*******************************************************************************************************/
    /*  NO NON-SINGLETON INITIALIZATION CODE BELOW THIS POINT                                              */
    /*******************************************************************************************************/


    /**
     * *** THE SINGLETON FIELD NEEDS TO BE LAST FIELD DEFINED ****<br />
     * So we put it at the end of the class to be sure
     * All static fields/properties need to be initialized prior to the constructor running
     * If the singleton field (or a static init block) is put in the middle of the field declarations
     * (which can happen when the IDE automatically creates new Fields at the end of the list) subtle
     * and very hard to track down bugs can be introduced.
     */
    private static final BallomaticPidSubsystem singleton = new BallomaticPidSubsystem();


    /**
     * Gets the single instance of this subsystem. Use of this method replaces the use
     * of a constructor such as <tt>new BallomaticSubsystem()</tt> in order to use ensure
     * only a single instance of a Subsystem is created. This is known as a Singleton.
     * For example, instead of doing this:
     * <pre>
     *     BallomaticSubsystem subsystem = new BallomaticSubsystem();
     * </pre>
     * do this:
     * <pre>
     *     BallomaticSubsystem subsystem = BallomaticSubsystem.getInstance();
     * </pre>
     *
     * @return the single instance of this subsystem.
     */
    public static BallomaticPidSubsystem getInstance() {return singleton;}

    /*******************************************************************************************************/
    /*  NO CODE BELOW THIS POINT                                                                           */
    /*******************************************************************************************************/

}
