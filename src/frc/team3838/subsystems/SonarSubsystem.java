package frc.team3838.subsystems;


import edu.wpi.first.wpilibj.AnalogInput;
import edu.wpi.first.wpilibj.DigitalInput;
import edu.wpi.first.wpilibj.DigitalOutput;
import edu.wpi.first.wpilibj.Ultrasonic;
import edu.wpi.first.wpilibj.command.Subsystem;
import frc.team3838.commands.sonar.SonarTestingCommand;
import frc.team3838.config.AIOs;
import frc.team3838.config.DIOs;
import frc.team3838.config.Enablings;



public class SonarSubsystem extends Subsystem
{
    @SuppressWarnings("UnusedDeclaration")
    private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(SonarSubsystem.class);

    /**
     * Multiplication factor to convert AnalogInput ultrasonic/sonar sensor values to a distance in inches.
     * Example:
     * <pre>double currentDistanceInInches = sonarAnalog.getValue() * SONAR_READING_TO_INCHES_FACTOR;</pre>
     */
    private static final double SONAR_READING_TO_INCHES_FACTOR = 0.125; //

    // We've found that the analog sonar is fair up to 8 inches. Closer than 8 inches it becomes very inaccurate.
    private AnalogInput sonarAnalog;
    private Ultrasonic ultrasonicDigital;



    /**
     * This is a private constructor to ensure a single instance (i.e. singleton pattern).
     * Use the static {@link #getInstance()} method to obtain a reference to the subsystem.
     * For example, instead of doing this:
     * <pre>
     *     SonarSubsystem subsystem = new SonarSubsystem();
     * </pre>
     * do this:
     * <pre>
     *     SonarSubsystem subsystem = SonarSubsystem.getInstance();
     * </pre>
     */
    private SonarSubsystem()
    {
        if (isEnabled())
        {
            try
            {
                logger.debug("Initializing {}", getClass().getSimpleName());

                sonarAnalog = new AnalogInput(AIOs.ANALOG_ULTRASONIC);

                DigitalOutput ultrasonicPingDigitalOutput = new DigitalOutput(DIOs.ULTRASONIC_PING_CHANNEL_DO);
                DigitalInput ultrasonicEchoDigitalInput =  new DigitalInput(DIOs.ULTRASONIC_ECHO_CHANNEL_DI);
                ultrasonicDigital = new Ultrasonic(ultrasonicPingDigitalOutput, ultrasonicEchoDigitalInput);
                ultrasonicDigital.setAutomaticMode(true);
                ultrasonicDigital.setEnabled(true);



                logger.debug("{} initialization completed successfully", getClass().getSimpleName());

            }
            catch (Exception e)
            {
                Enablings.isSonarSubsystemEnabled = false;
                logger.error("An exception occurred in {} constructor", getClass().getSimpleName(), e);
            }
        }
        else
        {
            logger.warn("{} is disabled and will not be initialized", getClass().getSimpleName());
        }
    }



    public double readDistanceForDigitalInMM()
    {
        return ultrasonicDigital.getRangeMM();
    }

    public double readDistanceForDigitalSonarInInches()
    {
        return ultrasonicDigital.getRangeInches();
    }

    public double readDistanceForAnalogSonar()
    {
        final double currentDistanceInInches = sonarAnalog.getValue() * SONAR_READING_TO_INCHES_FACTOR;//sensor returns a value from 0-4095 that is scaled to inches
        return currentDistanceInInches;
    }


    @Override
    public void initDefaultCommand()
    {
        if (isEnabled())
        {
            setDefaultCommand(new SonarTestingCommand());
        }
        logger.trace("{}.initDefaultCommand() completed", getClass().getSimpleName());
    }


    public static boolean isEnabled()
    {
        return Enablings.isSonarSubsystemEnabled;
    }

    /*******************************************************************************************************/
    /*  NO NON-SINGLETON INITIALIZATION CODE BELOW THIS POINT                                              */
    /*******************************************************************************************************/


    /**
     * *** THE SINGLETON FILED NEEDS TO BE LAST FIELD DEFINED ****<br />
     * So we put it at the end of the class to be sure
     * All static fields/properties need to be initialized prior to the constructor running
     * If the singleton field (or a static init block) is put in the middle of the field declarations
     * (which can happen when the IDE automatically creates new Fields at the end of the list) subtle
     * and very hard to track down bugs can be introduced.
     */
    private static final SonarSubsystem singleton = new SonarSubsystem();


    /**
     * Gets the single instance of this subsystem. Use of this method replaces the use
     * of a constructor such as <tt>new SonarSubsystem()</tt> in order to use ensure
     * only a single instance of a Subsystem is created. This is known as a Singleton.
     * For example, instead of doing this:
     * <pre>
     *     SonarSubsystem subsystem = new SonarSubsystem();
     * </pre>
     * do this:
     * <pre>
     *     SonarSubsystem subsystem = SonarSubsystem.getInstance();
     * </pre>
     *
     * @return the single instance of this subsystem.
     */
    public static SonarSubsystem getInstance() {return singleton;}

    /*******************************************************************************************************/
    /*  NO CODE BELOW THIS POINT                                                                           */
    /*******************************************************************************************************/

}
