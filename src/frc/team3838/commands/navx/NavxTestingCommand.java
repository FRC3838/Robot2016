package frc.team3838.commands.navx;

import com.kauailabs.navx.frc.AHRS;

import edu.wpi.first.wpilibj.command.Command;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc.team3838.subsystems.NavxSubsystem;
import frc.team3838.utils.MathUtils;



public class NavxTestingCommand extends Command
{
    @SuppressWarnings("UnusedDeclaration")
    private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(NavxTestingCommand.class);


    // *** All Subsystem classes MUST be Singletons and obtained via a static getInstance() method. ***
    // Instances should also be static to ensure they are initialized prior to the command

    protected static final NavxSubsystem navxSubsystem = NavxSubsystem.getInstance();


    public NavxTestingCommand()
    {
        // Use requires() here to declare subsystem dependencies
        // which must be declared as (static) fields be singletons
        // and obtained via a static getInstance() method
        // eg. requires(driveTrainSubsystem);
        //     requires(shooterSubsystem);
        requires(navxSubsystem);
    }


    // Called just before this Command runs the first time
    @Override
    protected void initialize()
    {
        //noinspection StatementWithEmptyBody
        if (areAllSubsystemsAreEnabled())
        {
            logger.debug("{}.initialize() called", getClass().getSimpleName());
        }
        else
        {
            logger.warn("Not all required subsystems for {} are enabled, thus the command can not be and will not be initialized or executed.", getClass().getSimpleName());
        }
    }


    static float lastRoll = Float.NaN;

    // Called repeatedly when this Command is scheduled to run (until isFinished() returns true)
    @Override
    protected void execute()
    {
        logger.trace("{}.execute() called", getClass().getSimpleName());
        if (areAllSubsystemsAreEnabled())
        {
            logger.trace("Executing Command '{}' ", getClass().getSimpleName());
            try
            {
                final AHRS navx = navxSubsystem.getNavx();
                if (navx != null)
                {
//                    SmartDashboard.putString("NAVx altitude",       MathUtils.formatNumber(navx.getAltitude(), 4));
//                    SmartDashboard.putString("NAVx angle",          MathUtils.formatNumber(navx.getAngle(), 4));
//                    SmartDashboard.putString("NAVx CompassHeading", MathUtils.formatNumber(navx.getCompassHeading(), 4));
//                    SmartDashboard.putString("NAVx Fused Heading",  MathUtils.formatNumber(navx.getFusedHeading(), 4));
//                    SmartDashboard.putString("NAVx Rate",           MathUtils.formatNumber(navx.getRate(), 4));
//                    SmartDashboard.putString("NAVx Pitch",          MathUtils.formatNumber(navx.getPitch(), 4));
//                    SmartDashboard.putString("NAVx Roll",           MathUtils.formatNumber(navx.getRoll(), 4));
                    SmartDashboard.putString("NAVx Yaw",            MathUtils.formatNumber(navx.getYaw(), 4));
                }
            }
            catch (Exception e)
            {
                logger.error("An exception occurred in {}.execute()", getClass().getSimpleName(), e);
            }
        }
        else
        {
            logger.trace("Not all required subsystems for {} are enabled. The command can not be and will not be executed.", getClass().getSimpleName());
        }
    }


    // Make this return true when this Command no longer needs to run execute()
    @Override
    protected boolean isFinished()
    {
        logger.trace("{}.isFinished() called", getClass().getSimpleName());
        return false;
    }


    protected static boolean areAllSubsystemsAreEnabled()
    {
        return (NavxSubsystem.isEnabled());
    }


    // Called once after isFinished returns true
    // do any clean up or post command work here
    @Override
    protected void end()
    {
        logger.trace("{}.end() called", getClass().getSimpleName());
    }


    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    @Override
    protected void interrupted()
    {
        logger.debug("{}.interrupted() called", getClass().getSimpleName());
    }
}
