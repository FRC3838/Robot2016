package frc.team3838.commands.autonomous.groups;


import frc.team3838.commands.autonomous.AutonomousDisableMotorSafetyCommand;
import frc.team3838.commands.autonomous.AutonomousDriveOverRoughObstacleCommand;
import frc.team3838.commands.autonomous.AutonomousEnableMotorSafetyCommand;
import frc.team3838.commands.ballomatic.BallomaticMoveToPickPosition;
import frc.team3838.commands.drive.DrivePidRotateCommand;
import frc.team3838.commands.drive.DriveSetDistanceCommand;
import frc.team3838.commands.drive.StopDrivingCommand;
import frc.team3838.commands.standardCommands.SleepCommand;
import frc.team3838.commands.standardCommands.SleepCommand.PartialSeconds;



public class AutonomousDriveUnderLowBarAndGoForwardCommandGroup extends AutonomousAbstractCommandGroup
{
    public AutonomousDriveUnderLowBarAndGoForwardCommandGroup() { super(); }


    public AutonomousDriveUnderLowBarAndGoForwardCommandGroup(String name) { super(name); }


    @Override
    protected void addCommands()
    {
        addSequential(new AutonomousDisableMotorSafetyCommand());
        addSequential(new BallomaticMoveToPickPosition());
        addSequential(SleepCommand.createSeconds(0, PartialSeconds.QuarterSecond));

        addSequential(new AutonomousDriveOverRoughObstacleCommand());
        addSequential(new StopDrivingCommand());

        addSequential(DriveSetDistanceCommand.forwardFeetAndInches(7, 8));
        addSequential(SleepCommand.createSeconds(0, PartialSeconds.QuarterSecond));

        addSequential(new DrivePidRotateCommand(60.0));
        addSequential(SleepCommand.createSeconds(0, PartialSeconds.QuarterSecond));

        addSequential(DriveSetDistanceCommand.forwardFeetAndInches(2, 0));
        addSequential(SleepCommand.createSeconds(0, PartialSeconds.QuarterSecond));

        //addSequential(new BallomaticMoveToMidPosition());
        addSequential(new AutonomousEnableMotorSafetyCommand());
    }
}