package frc.team3838.commands.autonomous.groups;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.ImmutableList;

import frc.team3838.commands.autonomous.AutonomousDisableMotorSafetyCommand;
import frc.team3838.commands.autonomous.AutonomousEnableMotorSafetyCommand;
import frc.team3838.commands.drive.DrivePidRotateCommand;



public class AutonomousDrivePidRotateCommandGroup extends AutonomousAbstractCommandGroup
{
    private static final Logger logger = LoggerFactory.getLogger(AutonomousDrivePidRotateCommandGroup.class);

    /**
     * The angle to rotate, relative to zero at NavxReset time. In other words,
     * this is the angle to rotate <strong><em>TO</em></strong>, now how much to
     * rotate to. If the Robot is at 90° (from its start position or last Navx Reset)
     * and you pass in 90°, it will NOT rotate. And if the Robot is at 90° (from its
     * start position or last Navx Reset), and you pass in 180° it will rotate 90°
     * (or possibly 270°) to the 180° point. Value must be in the range of
     * -180° to 180
     */
    private double targetAngle;


    /**
     * @param targetAngle the angle to rotate, relative to zero at NavxReset time. In other words,
     *                          this is the angle to rotate <strong><em>TO</em></strong>, now how much to
     *                          rotate to. If the Robot is at 90° (from its start position or last Navx Reset)
     *                          and you pass in 90°, it will NOT rotate. And if the Robot is at 90° (from its
     *                          start position or last Navx Reset), and you pass in 180° it will rotate 90°
     *                          (or possibly 270°) to the 180° point. Value must be in the range of
     *                          -180° to 180
     */
    public AutonomousDrivePidRotateCommandGroup(int targetAngle)
    {
        this((double) targetAngle);
    }

    /**
     * @param targetAngle the angle to rotate, relative to zero at NavxReset time. In other words,
     *                          this is the angle to rotate <strong><em>TO</em></strong>, now how much to
     *                          rotate to. If the Robot is at 90° (from its start position or last Navx Reset)
     *                          and you pass in 90°, it will NOT rotate. And if the Robot is at 90° (from its
     *                          start position or last Navx Reset), and you pass in 180° it will rotate 90°
     *                          (or possibly 270°) to the 180° point. Value must be in the range of
     *                          -180° to 180
     */
    public AutonomousDrivePidRotateCommandGroup(double targetAngle)
    {
        this(AutonomousDrivePidRotateCommandGroup.class.getSimpleName() + '_' + targetAngle, targetAngle);
    }


    /**
     * @param targetAngle the angle to rotate, relative to zero at NavxReset time. In other words,
     *                          this is the angle to rotate <strong><em>TO</em></strong>, now how much to
     *                          rotate to. If the Robot is at 90° (from its start position or last Navx Reset)
     *                          and you pass in 90°, it will NOT rotate. And if the Robot is at 90° (from its
     *                          start position or last Navx Reset), and you pass in 180° it will rotate 90°
     *                          (or possibly 270°) to the 180° point. Value must be in the range of
     *                          -180° to 180
     */
    public AutonomousDrivePidRotateCommandGroup(String name, double targetAngle)
    {
        super(name, ImmutableList.of(
            new AutonomousDisableMotorSafetyCommand(),
            new DrivePidRotateCommand(targetAngle),
            new AutonomousEnableMotorSafetyCommand() ));
        logger.debug("{} created with target angle of {} for desired angle of {}", name, this.targetAngle, targetAngle);
    }


    @Override
    protected void addCommands()
    {
        //commands are added via special constructor
    }
}
