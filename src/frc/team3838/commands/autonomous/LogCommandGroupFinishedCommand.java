package frc.team3838.commands.autonomous;

import java.time.Duration;
import java.time.LocalTime;

import edu.wpi.first.wpilibj.command.Command;



public class LogCommandGroupFinishedCommand extends AbstractLogCommandGroupStatusCommand
{

    private AbstractLogCommandGroupStatusCommand startStatusLogger;

    public LogCommandGroupFinishedCommand(Command commandToLog)
    {
        super(commandToLog);
    }


    public LogCommandGroupFinishedCommand(Command commandToLog, AbstractLogCommandGroupStatusCommand startStatusLogger)
    {
        super(commandToLog);
        this.startStatusLogger = startStatusLogger;
    }


    @Override
    protected String getStatus() {return "Finished";}


    @Override
    protected String getAdditionalMessage()
    {
        if (startStatusLogger != null)
        {
            final LocalTime startTime = startStatusLogger.getStatusTime();
            final Duration duration = Duration.between(startTime, statusTime);


            return " a duration of " + duration;
        }
        else
        {
            return super.getAdditionalMessage();
        }
    }
}
