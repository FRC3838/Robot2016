package frc.team3838.commands.drive;

import org.apache.commons.lang3.tuple.ImmutablePair;

import edu.wpi.first.wpilibj.command.Command;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc.team3838.subsystems.DriveTrainSubsystem;



public class DriveForwardForSetTimeCommand extends Command
{
    @SuppressWarnings("UnusedDeclaration")
    private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(DriveForwardForSetTimeCommand.class);


    // *** All Subsystem classes MUST be Singletons and obtained via a static getInstance() method. ***
    // Instances should also be static to ensure they are initialized prior to the command

    protected static final DriveTrainSubsystem driveTrainSubsystem = DriveTrainSubsystem.getInstance();

    long startTime;
    private final long  durationInMs;



    public DriveForwardForSetTimeCommand(long durationInMs)
    {
        // Use requires() here to declare subsystem dependencies
        // which must be declared as (static) fields be singletons
        // and obtained via a static getInstance() method
        // eg. requires(driveTrainSubsystem);
        //     requires(shooterSubsystem);
        requires(driveTrainSubsystem);
        this.durationInMs = durationInMs;
    }


    // Called just before this Command runs the first time
    @Override
    protected void initialize()
    {
        //noinspection StatementWithEmptyBody
        if (areAllSubsystemsAreEnabled())
        {
            logger.debug("{}.initialize() called", getClass().getSimpleName());
            startTime = System.currentTimeMillis();
        }
        else
        {
            logger.warn("Not all required subsystems for {} are enabled, thus the command can not be and will not be initialized or executed.", getClass().getSimpleName());
        }
    }


    // Called repeatedly when this Command is scheduled to run (until isFinished() returns true)
    @Override
    protected void execute()
    {
        logger.trace("{}.execute() called", getClass().getSimpleName());
        if (areAllSubsystemsAreEnabled())
        {
            logger.trace("Executing Command '{}' ", getClass().getSimpleName());
            try
            {
//                final double speed = DriveTrainSubsystem.readAutoSpeed();
                final double speed = 0.6;
                final ImmutablePair<Double, Double> speeds = DriveTrainSubsystem.adjustForPull(speed);
                double speedL = speeds.getLeft();
                double speedR = speeds.getRight();
                logger.debug("Calling driveTrainSubsystem.driveViaTankDrive() with speed of L: {}, R: {}", speedL, speedR);
                driveTrainSubsystem.driveViaTankDrive(speedL, speedR);
            }
            catch (Exception e)
            {
                logger.error("An exception occurred in {}.execute()", getClass().getSimpleName(), e);
            }
        }
        else
        {
            logger.trace("Not all required subsystems for {} are enabled. The command can not be and will not be executed.", getClass().getSimpleName());
        }
    }


    // Make this return true when this Command no longer needs to run execute()
    @Override
    protected boolean isFinished()
    {
        logger.trace("{}.isFinished() called", getClass().getSimpleName());
//        return (System.currentTimeMillis() - startTime) >= durationInMs;

        // FIX BEFORE COMMIT
        return (System.currentTimeMillis() - startTime) >= (long) SmartDashboard.getNumber("Auto Drive Duration (ms)", 3_000);
    }


    protected static boolean areAllSubsystemsAreEnabled()
    {
        return (DriveTrainSubsystem.isEnabled());
    }


    // Called once after isFinished returns true
    // do any clean up or post command work here
    @Override
    protected void end()
    {
        logger.trace("{}.end() called", getClass().getSimpleName());
    }


    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    @Override
    protected void interrupted()
    {
        logger.debug("{}.interrupted() called", getClass().getSimpleName());
    }
}
