package frc.team3838.commands.drive;

import edu.wpi.first.wpilibj.command.Command;
import frc.team3838.subsystems.DriveTrainSubsystem;
import frc.team3838.subsystems.DriveTrainSubsystem.Direction;



public class DriveSetDistanceCommand extends Command
{
    @SuppressWarnings("UnusedDeclaration")
    private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(DriveSetDistanceCommand.class);

    private final Direction direction;
    private final double distanceToTravelInInches;

    private DriveTrainSubsystem driveTrainSubsystem = DriveTrainSubsystem.getInstance();

    public static DriveSetDistanceCommand inches(Direction direction, double distanceToTravelInInches)
    {
        return new DriveSetDistanceCommand(direction, distanceToTravelInInches);
    }


    public static DriveSetDistanceCommand feet(Direction direction, double distanceToTravelInFeet)
    {
        return new DriveSetDistanceCommand(direction, distanceToTravelInFeet * 12);
    }


    /**
     * @param distanceToTravel the distance to travel written as {@code 6"} of inches and {@code 3'} for feet
     *
     * @return a configured DriveSetDistanceCommand command
     */
    public static DriveSetDistanceCommand forward(String distanceToTravel)
    {
        distanceToTravel = distanceToTravel.trim();
        final char unit = distanceToTravel.charAt(distanceToTravel.length() - 1);
        if (unit == '\'')
        {
            return new DriveSetDistanceCommand(Direction.FORWARD, Double.parseDouble(distanceToTravel.substring(0, distanceToTravel.length() - 1)) * 12);
        }
        else if (unit == '"')
        {
            return new DriveSetDistanceCommand(Direction.FORWARD, Double.parseDouble(distanceToTravel.substring(0, distanceToTravel.length() - 1)));
        }
        else
        {
            throw new IllegalArgumentException("Could not parse " + distanceToTravel + " as a distance in inches or feet.");
        }
    }


    public static DriveSetDistanceCommand forwardInches(double distanceToTravelInInches)
    {
        return new DriveSetDistanceCommand(Direction.FORWARD, distanceToTravelInInches);
    }


    public static DriveSetDistanceCommand forwardFeet(double distanceToTravelInFeet)
    {
        return new DriveSetDistanceCommand(Direction.FORWARD, distanceToTravelInFeet * 12);
    }


    public static DriveSetDistanceCommand forwardFeetAndInches(double distanceToTravelInFeet, double additionalInches)
    {
        return new DriveSetDistanceCommand(Direction.FORWARD, (distanceToTravelInFeet * 12) + additionalInches);
    }

    /**
     * @param distanceToTravel the distance to travel written as {@code 6"} of inches and {@code 3'} for feet
     *
     * @return a configured DriveSetDistanceCommand command
     */
    public static DriveSetDistanceCommand reverse(String distanceToTravel)
    {
        distanceToTravel = distanceToTravel.trim();
        final char unit = distanceToTravel.charAt(distanceToTravel.length() - 1);
        if (unit == '\'')
        {
            return new DriveSetDistanceCommand(Direction.REVERSE, Double.parseDouble(distanceToTravel.substring(0, distanceToTravel.length() - 1)) * 12);
        }
        else if (unit == '"')
        {
            return new DriveSetDistanceCommand(Direction.REVERSE, Double.parseDouble(distanceToTravel.substring(0, distanceToTravel.length() - 1)));
        }
        else
        {
            throw new IllegalArgumentException("Could not parse " + distanceToTravel + " as a distance in inches or feet.");
        }
    }


    public static DriveSetDistanceCommand reverseInches(double distanceToTravelInInches)
    {
        return new DriveSetDistanceCommand(Direction.REVERSE, distanceToTravelInInches);
    }


    public static DriveSetDistanceCommand reverseFeet(double distanceToTravelInFeet)
    {
        return new DriveSetDistanceCommand(Direction.REVERSE, distanceToTravelInFeet * 12);
    }


    public static DriveSetDistanceCommand reverseFeetAndInches(double distanceToTravelInFeet, double additionalInches)
    {
        return new DriveSetDistanceCommand(Direction.REVERSE, (distanceToTravelInFeet * 12) + additionalInches);
    }


    /**
     * @param distanceToTravel the distance to travel written as {@code 6"} of inches and {@code 3'} for feet
     *
     * @return a configured DriveSetDistanceCommand command
     */
    public static DriveSetDistanceCommand left(String distanceToTravel)
    {
        distanceToTravel = distanceToTravel.trim();
        final char unit = distanceToTravel.charAt(distanceToTravel.length() - 1);
        if (unit == '\'')
        {
            return new DriveSetDistanceCommand(Direction.LEFT, Double.parseDouble(distanceToTravel.substring(0, distanceToTravel.length() - 1)) * 12);
        }
        else if (unit == '"')
        {
            return new DriveSetDistanceCommand(Direction.LEFT, Double.parseDouble(distanceToTravel.substring(0, distanceToTravel.length() - 1)));
        }
        else
        {
            throw new IllegalArgumentException("Could not parse " + distanceToTravel + " as a distance in inches or feet.");
        }
    }


    public static DriveSetDistanceCommand leftInches(double distanceToTravelInInches)
    {
        return new DriveSetDistanceCommand(Direction.LEFT, distanceToTravelInInches);
    }


    public static DriveSetDistanceCommand leftFeet(double distanceToTravelInFeet)
    {
        return new DriveSetDistanceCommand(Direction.LEFT, distanceToTravelInFeet * 12);
    }


    /**
     * @param distanceToTravel the distance to travel written as {@code 6"} of inches and {@code 3'} for feet
     *
     * @return a configured DriveSetDistanceCommand command
     */
    public static DriveSetDistanceCommand right(String distanceToTravel)
    {
        distanceToTravel = distanceToTravel.trim();
        final char unit = distanceToTravel.charAt(distanceToTravel.length() - 1);
        if (unit == '\'')
        {
            return new DriveSetDistanceCommand(Direction.RIGHT, Double.parseDouble(distanceToTravel.substring(0, distanceToTravel.length() - 1)) * 12);
        }
        else if (unit == '"')
        {
            return new DriveSetDistanceCommand(Direction.RIGHT, Double.parseDouble(distanceToTravel.substring(0, distanceToTravel.length() - 1)));
        }
        else
        {
            throw new IllegalArgumentException("Could not parse " + distanceToTravel + " as a distance in inches or feet.");
        }
    }


    public static DriveSetDistanceCommand rightInches(double distanceToTravelInInches)
    {
        return new DriveSetDistanceCommand(Direction.RIGHT, distanceToTravelInInches);
    }


    public static DriveSetDistanceCommand rightFeet(double distanceToTravelInFeet)
    {
        return new DriveSetDistanceCommand(Direction.RIGHT, distanceToTravelInFeet * 12);
    }


    public DriveSetDistanceCommand(Direction direction, double distanceToTravelInInches)
    {
        requires(driveTrainSubsystem);
        this.distanceToTravelInInches = distanceToTravelInInches;
        this.direction = direction;
    }


    // Called just before this Command runs the first time
    @Override
    protected void initialize()
    {
        //noinspection StatementWithEmptyBody
        if (allSubsystemsAreEnabled())
        {
            driveTrainSubsystem.resetEncoders();
        }
        else
        {
            logger.info("Not all required subsystems for DriveSetDistanceCommand are enabled. The command can not be and will not be initialized or executed.");
        }
    }


    protected double getDistanceReadingAbsoluteValue()
    {
        switch (direction)
        {
            case FORWARD:
            case REVERSE:
                return Math.abs(driveTrainSubsystem.getAverageDistanceInInches());
//            case RIGHT:
//            case LEFT:
//                return Math.abs(driveTrainSubsystem.getSideMovementDistance());
        }
        return Double.NaN;
    }


    protected double getRemainingDistance()
    {
        return distanceToTravelInInches - getDistanceReadingAbsoluteValue();
    }


    protected boolean hasReachTarget()
    {
        return getDistanceReadingAbsoluteValue() >= (distanceToTravelInInches - 1);
    }


    // Called repeatedly when this Command is scheduled to run (until isFinished() returns true)
    @Override
    protected void execute()
    {
        if (allSubsystemsAreEnabled())
        {
            try
            {

                if (hasReachTarget())
                {
                    driveTrainSubsystem.stop();
                }
                else
                {
                    //The drive command will ultimately scale the speed between STALL_SPEED and MAX_SPEED.
                    //So use real desired percentages here
                    double speed;

                    final double remainingDistance = getRemainingDistance();

                    if (remainingDistance > 180)
                    {
                        speed = 1;
                    }
                    else if (remainingDistance > 120)
                    {
                        speed = 0.75;
                    }
                    else if (remainingDistance > 96)
                    {
                        speed = 0.6;
                    }
                    else if (remainingDistance > 72)
                    {
                        speed = 0.5;
                    }
                    else if (remainingDistance > 60)
                    {
                        speed = 0.4;
                    }
                    else if (remainingDistance > 48)
                    {
                        speed = 0.3;
                    }
                    else if (remainingDistance > 24)
                    {
                        speed = 0.2;
                    }
                    else if (remainingDistance > 18)
                    {
                        speed = 0.10;
                    }
                    else if ((remainingDistance < 2) && (distanceToTravelInInches > 18))
                    {
                        speed = 0.05;
                    }
                    else
                    {
                        speed = 0.55;
                    }
//                    else if (remainingDistance > 12)
//                    {
//                        speed = 0.50;
//                    }
//                    else if (remainingDistance > 8)
//                    {
//                        speed = 0.25;
//                    }
//                    else if (remainingDistance < 3)
//                    {
//                        speed = 0.10;
//                    }
//                    else if (remainingDistance <= 0)
//                    {
//                        speed = 0;
//                    }
                    logger.trace("Speed set to {}", speed);
                    driveTrainSubsystem.drive(direction, speed);
                }
            }
            catch (Exception e)
            {
                logger.error("An exception occurred in DriveSetDistanceCommand.execute()", e);
            }
        }
        else
        {
            logger.trace("Not all required subsystems for DriveSetDistanceCommand are enabled. The command can not be and will not be executed.");
        }
    }


    // Make this return true when this Command no longer needs to run execute()
    @Override
    protected boolean isFinished()
    {
        return hasReachTarget();
    }


    protected boolean allSubsystemsAreEnabled()
    {
        return (DriveTrainSubsystem.isEnabled());
    }


    // Called once after isFinished returns true
    // do any clean up or post command work here
    @Override
    protected void end()
    {
        driveTrainSubsystem.stop();
    }


    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    @Override
    protected void interrupted()
    {

    }
}
