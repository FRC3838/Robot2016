package frc.team3838.commands.drive;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import edu.wpi.first.wpilibj.command.Command;
import frc.team3838.subsystems.DriveTrainSubsystem;
import frc.team3838.subsystems.DriveTrainSubsystem.RotationDirection;
import frc.team3838.subsystems.NavxSubsystem;
import frc.team3838.utils.MathUtils;



public class DriveRotateCommand extends Command
{
    @SuppressWarnings("UnusedDeclaration")
    private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(DriveRotateCommand.class);

    // Used example code from http://www.pdocs.kauailabs.com/navx-mxp/examples/rotate-to-angle-2/

    // *** All Subsystem classes MUST be Singletons and obtained via a static getInstance() method. ***
    // Instances should also be static to ensure they are initialized prior to the command

    protected static final DriveTrainSubsystem driveTrainSubsystem = DriveTrainSubsystem.getInstance();
    protected static final NavxSubsystem navxSubsystem = NavxSubsystem.getInstance();

    private ScheduledExecutorService scheduledExecutorService;

    protected final RotationDirection rotationDirection;
    protected final float degreesToRotate;
    protected final float targetAngle;
    protected float startingAngle;
    protected long startTime;

    protected boolean hasReachedTarget = false;


    public DriveRotateCommand(RotationDirection rotationDirection, float degreesToRotate)
    {
        // Use requires() here to declare subsystem dependencies
        // which must be declared as (static) fields be singletons
        // and obtained via a static getInstance() method
        // eg. requires(driveTrainSubsystem);
        //     requires(shooterSubsystem);
        requires(driveTrainSubsystem);
        requires(navxSubsystem);
        this.rotationDirection = rotationDirection;
        this.degreesToRotate = degreesToRotate;
        this.startingAngle = readYaw();
        this.targetAngle = this.startingAngle + degreesToRotate;

        logger.info("{} created. degrees = {}; startingAngle = {}; targetAngle = {}", getName(), degreesToRotate, startingAngle, targetAngle);
        if (!NavxSubsystem.isEnabled() || (navxSubsystem.getNavx() == null))
        {
            throw new IllegalStateException("The NavxSubsystem is disabled. This command requires the NAVx");
        }
    }


    private static float readYaw()
    {
        return (navxSubsystem.getNavx() != null) ? navxSubsystem.getNavx().getYaw() : Float.NaN;
    }


    // Called just before this Command runs the first time
    @Override
    protected void initialize()
    {
        startTime = System.currentTimeMillis();
        //noinspection StatementWithEmptyBody
        if (areAllSubsystemsAreEnabled())
        {
            logger.debug("{}.initialize() called", getClass().getSimpleName());
            hasReachedTarget = false;
            scheduledExecutorService  = Executors.newScheduledThreadPool(1);

            scheduledExecutorService.scheduleAtFixedRate((Runnable) () ->
            {
                hasReachedTarget = MathUtils.isBetween(Math.abs(readYaw()), Math.abs(targetAngle) - 0.5, Math.abs(targetAngle + 0.5));
                if (hasReachedTarget)
                {
                    driveTrainSubsystem.stop();
                }
            }, 1, 1, TimeUnit.MILLISECONDS);

            startingAngle = readYaw();
            logger.debug("startingAngle = {}", startingAngle);
        }
        else
        {
            logger.warn("Not all required subsystems for {} are enabled, thus the command can not be and will not be initialized or executed.", getClass().getSimpleName());
        }
    }




    // Called repeatedly when this Command is scheduled to run (until isFinished() returns true)
    @Override
    protected void execute()
    {
        logger.trace("{}.execute() called", getClass().getSimpleName());
        if (areAllSubsystemsAreEnabled())
        {
            logger.trace("Executing Command '{}' ", getClass().getSimpleName());
            try
            {
                double speed = 0.75;
                if (Math.abs(Math.abs(targetAngle) - Math.abs(readYaw())) <= 8)
                {
                    speed = .6;
                }

                if (hasReachedTarget)
                {
                    driveTrainSubsystem.stop();
                }
                else
                {
                    // Clockwise: Left:Neg  Right:Pos
                    // CounterClockwise: Left:Pos  Right:Neg
                    driveTrainSubsystem.getRobotDrive().tankDrive(-speed, speed);
                    logger.trace("currentAngle = {}", readYaw());
                }
            }
            catch (Exception e)
            {
                logger.error("An exception occurred in {}.execute()", getClass().getSimpleName(), e);
            }
        }
        else
        {
            logger.trace("Not all required subsystems for {} are enabled. The command can not be and will not be executed.", getClass().getSimpleName());
        }
    }


    // Make this return true when this Command no longer needs to run execute()
    @Override
    protected boolean isFinished()
    {
        return hasReachedTarget;
    }


    protected static boolean areAllSubsystemsAreEnabled()
    {
        return (DriveTrainSubsystem.isEnabled() && NavxSubsystem.isEnabled());
    }


    // Called once after isFinished returns true
    // do any clean up or post command work here
    @Override
    protected void end()
    {
        logger.trace("{}.end() called", getClass().getSimpleName());
        driveTrainSubsystem.stop();

        try
        {
            scheduledExecutorService.shutdownNow();
        }
        catch (Exception ignore) { }
    }


    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    @Override
    protected void interrupted()
    {
        logger.debug("{}.interrupted() called", getClass().getSimpleName());
    }



}
