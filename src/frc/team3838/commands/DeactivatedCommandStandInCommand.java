package frc.team3838.commands;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.wpi.first.wpilibj.command.Command;



public class DeactivatedCommandStandInCommand extends Command
{
    @SuppressWarnings("UnusedDeclaration")
    private static final Logger logger = LoggerFactory.getLogger(DeactivatedCommandStandInCommand.class);

    private final Class<? extends Command> commandClass;
    private final String logMsg;
    private boolean messageLogged = false;

    public DeactivatedCommandStandInCommand(Class<? extends Command> commandClass)
    {
        this.commandClass = commandClass;
        this.logMsg = String.format("The \"No Action\" Stand In command execution has been called in place of the %s. "
                                    + "This is likely because the because subsystem the command typically assigned to this action uses has been disabled.", commandClass.getSimpleName());
    }


    // Called just before this Command runs the first time
    @Override
    protected void initialize()
    {
        logger.info("The \"No Action\" Stand In command is being assigned in place of the {}. "
                    + "This is likely because the because subsystem the command typically assigned to this action uses has been disabled.", commandClass.getSimpleName());
    }


    // Called repeatedly when this Command is scheduled to run (until isFinished() returns true)
    @Override
    protected void execute()
    {
        if (!messageLogged)
        {
            logger.info(logMsg);
            messageLogged = true;
        }
        else
        {
            logger.trace(logMsg);
        }
    }


    // Make this return true when this Command no longer needs to run execute()
    @Override
    protected boolean isFinished()
    {
        return true;
    }


    // Called once after isFinished returns true
    // do any clean up or post command work here
    @Override
    protected void end()
    {
        //no op
    }


    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    @Override
    protected void interrupted()
    {

    }
}
