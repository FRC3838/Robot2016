package frc.team3838.commands;

import edu.wpi.first.wpilibj.command.Command;



/**
 * A No Operation (No Op) command that can be used as a placeholder.
 */
public final class NoOpCommand extends Command
{
    @SuppressWarnings("UnusedDeclaration")
    private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(NoOpCommand.class);


    public NoOpCommand() { }


    // Called just before this Command runs the first time
    @Override
    protected void initialize()
    {
        //no op
    }


    // Called repeatedly when this Command is scheduled to run (until isFinished() returns true)
    @Override
    protected void execute()
    {
        //no op
    }


    // Make this return true when this Command no longer needs to run execute()
    @Override
    protected boolean isFinished()
    {
        return true;
    }

    // Called once after isFinished returns true
    // do any clean up or post command work here
    @Override
    protected void end()
    {
        //no op
    }


    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    @Override
    protected void interrupted()
    {
        //no op
    }
}
