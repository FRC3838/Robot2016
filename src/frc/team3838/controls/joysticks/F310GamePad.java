package frc.team3838.controls.joysticks;

import java.util.SortedMap;
import java.util.TreeMap;

import com.google.common.collect.ImmutableSortedMap;

import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.buttons.Button;



public enum F310GamePad implements IJoystickConfig
{

    GREEN_A_DOWN
        {
            @Override
            public int getButtonNumber()
            {
                return 1;
            }
        },
    RED_B_RIGHT
        {
            @Override
            public int getButtonNumber()
            {
                return 2;
            }
        },
    BLUE_X_LEFT
        {
            @Override
            public int getButtonNumber()
            {
                return 3;
            }
        },
    YELLOW_Y_UP
        {
            @Override
            public int getButtonNumber()
            {
                return 4;
            }
        },
    LB
        {
            @Override
            public int getButtonNumber()
            {
                return 5;
            }
        },
    RB
        {
            @Override
            public int getButtonNumber()
            {
                return 6;
            }
        },
    BACK_LEFT_ARROW
        {
            @Override
            public int getButtonNumber()
            {
                return 7;
            }
        },
    START_RIGHT_ARROW
        {
            @Override
            public int getButtonNumber()
            {
                return 8;
            }
        },
    LEFT_JOY_BTN
        {
            @Override
            public int getButtonNumber()
            {
                return 9;
            }
        },
    RIGHT_JOY_BTN
        {
            @Override
            public int getButtonNumber()
            {
                return 10;
            }
        },
        ;

    public enum Axises
    {
        /*

            RawAxis Numbers of Thumb Joysticks
            Left
            X: 0
            Y: 1
            Left Throttle (LT) button: 2
            Right
            Right Throttle Button (RT) : 3
            X: 4
            Y: 5

         */

        /** The X axis of the Left Thumb Joystick.  Range is -1 to 1 with -1 to the left and 1 to the right.*/
        LEFT_X(0),
        /** The Y axis of the Left Thumb Joystick. Range is -1 to 1 with -1 to the front (away from the operator), 1 to the back (towards the operator). */
        LEFT_Y(1),
        /** The Left Throttle button -- labeled 'LT' -- on the back og the gamepad that is controlled by the index finger. */
        LEFT_THROTTLE(2),
        /** The Right Throttle button -- labeled 'RT' -- on the back og the gamepad that is controlled by the index finger. */
        RIGHT_THROTTLE(3),
        /** The X axis of the Right Thumb Joystick.  Range is -1 to 1 with -1 to the left and 1 to the right. */
        RIGHT_X(4),
        /** The Y axis of the Right Thumb Joystick. Range is -1 to 1 with -1 to the front (away from the operator), 1 to the back (towards the operator). */
        RIGHT_Y(5),
        ;
        final int rawAxis;


        Axises(int rawAxis) { this.rawAxis = rawAxis; }


        public int getRawAxis() { return rawAxis; }
    }

    public enum DPadButtons
    {

        /** The north, or up, button DPad/POV button. */
        N(false, true),
        /** The south, or down, button DPad/POV button. */
        S(false, false),
        /** The west, or left, button DPad/POV button. */
        W(true, false),
        /** The east, or right, button DPad/POV button. */
        E(true, true);

        final boolean isX;
        final boolean readsPositive;


        DPadButtons(boolean isX, boolean readsPositive)
        {
            this.isX = isX;
            this.readsPositive = readsPositive;
        }


        public Button getButton(JoystickDefinition<F310GamePad> joystickDefinition)
        {
            return new Button()
            {
                @Override
                public boolean get()
                {
                    final Joystick joystick = joystickDefinition.getJoystick();
                    double povDirection = joystick.getPOV(0);
                    if (povDirection == -1) { return false; }

                    double povValue  = isX ? Math.sin(Math.toRadians(povDirection)) : Math.cos(Math.toRadians(povDirection));

                    return (povValue * (readsPositive ? 1 : -1)) > 0.2;

                }
            };
        }

    }

    protected static ImmutableSortedMap<Integer, F310GamePad> map;


    static
    {
        final F310GamePad[] values = values();
        SortedMap<Integer, F310GamePad> map = new TreeMap<>();
        for (F310GamePad value : values)
        {
            map.put(value.getButtonNumber(), value);
        }
        F310GamePad.map = ImmutableSortedMap.copyOfSorted(map);
    }


    public static F310GamePad lookupByButtonNumber(int buttonNumber)
    {
        return map.get(buttonNumber);
    }


    @Override
    public String getJoystickTypeName()
    {
        return "Logitech F310 GamePad";
    }


    public static Button getAxisAsButton(Joystick joystick, Axises axis)
    {
        return new AxisToButton(joystick, axis.getRawAxis());
    }

    public static Button getAxisAsButton(JoystickDefinition<F310GamePad> joystickDef, Axises axis)
    {
        return new AxisToButton(joystickDef.getJoystick(), axis.getRawAxis());
    }

    public static Button getLeftThrottleAsButton(Joystick joystick)
    {
        return getAxisAsButton(joystick, Axises.LEFT_THROTTLE);
    }

    public static Button getLeftThrottleAsButton(JoystickDefinition<F310GamePad> joystickDef)
    {
        return getAxisAsButton(joystickDef, Axises.LEFT_THROTTLE);
    }

    public static Button getRightThrottleAsButton(Joystick joystick)
    {
        return getAxisAsButton(joystick, Axises.RIGHT_THROTTLE);
    }


    public static Button getRightThrottleAsButton(JoystickDefinition<F310GamePad> joystickDef)
    {
        return getAxisAsButton(joystickDef, Axises.RIGHT_THROTTLE);
    }
}
