package frc.team3838.utils.motors;


public enum DriveSensitivityCurve
{
    @SuppressWarnings("unused")
    STANDARD
        {
            @Override
            public double adjust(double value)
            {
                return value;
            }
        },
    @SuppressWarnings("unused")
    SQUARED
        {
            @Override
            public double adjust(double value) { return (value >= 0.0) ? (value * value) : -(value * value); }
        },
    @SuppressWarnings("unused")
    CUBED
        {
            @Override
            public double adjust(double value) { return value * value * value; }
        },
    @SuppressWarnings("unused")
    FOURTH_POWER
        {
            @Override
            public double adjust(double value)
            {
                return (value >= 0.0) ? (value * value * value * value) : -(value * value * value * value);
            }
        };

    public abstract double adjust(double value);
}
