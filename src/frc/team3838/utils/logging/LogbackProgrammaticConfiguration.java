package frc.team3838.utils.logging;


import java.util.Map.Entry;
import java.util.concurrent.TimeUnit;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import org.reflections.Reflections;
import org.slf4j.LoggerFactory;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableMap.Builder;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.encoder.PatternLayoutEncoder;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.ConsoleAppender;
import ch.qos.logback.core.status.ErrorStatus;
import ch.qos.logback.core.status.InfoStatus;
import ch.qos.logback.core.status.Status;
import ch.qos.logback.core.status.StatusManager;
import ch.qos.logback.core.status.WarnStatus;
import frc.team3838.Robot3838;
import frc.team3838.commands.winch.WinchRetractSlashPullInToRaiseRobotCommand;
import frc.team3838.controls.joysticks.Joysticks;

import static ch.qos.logback.classic.Level.*;



//TODO: See if a groovy configuration will work
@SuppressWarnings({"UseOfSystemOutOrSystemErr", "UnusedDeclaration", "CallToPrintStackTrace"})
public class LogbackProgrammaticConfiguration
{
    //@formatter:off
    /**
     * Boolean flag meant for use to guard Smartdashboard Debug data or other Debugging output.
     * <strong>Setting this value does NOT activate DEBUG logging.</strong> Use {@link #rootLevel}
     * to change logging output level.
     */
    public static final boolean IN_DEBUGGING_MODE = false;

    /** The ROOT logger level. */
    public static final Level rootLevel = INFO;

    /** If logging framework should output initialization debug information. */
    public static final boolean logInitializationDebug = false;

    private static final Builder<String, Level> levelSettingsMapBuilder = ImmutableMap.builder();



    // === SET INDIVIDUAL LOGGING LEVELS HERE ===
    static
    {
        set(LogTester.class, OFF);
        set("frc.team3838", WARN);
        // PRIMARY & DEBUG CLASSES
        set(Robot3838.class, INFO);
        set(Joysticks.class, WARN);

//        // SUBSYSTEMS
//        set(WinchSubsystem.class, TRACE);
//        set(DioTestingSubsystem.class, INFO);
//        set(DriveTrainSubsystem.class, INFO);
//        set(NavxSubsystem.class, WARN);
//        set(SonarTestingCommand.class, INFO);
//        set(BallomaticSubsystem.class, INFO);
//
//        // COMMANDS
//        setPackage(NoOpCommand.class.getPackage(), INFO);
        setPackage(WinchRetractSlashPullInToRaiseRobotCommand.class, INFO);
//        set(GyroAccelSubsystem.class, WARN);
//        set(NavxTestingCommand.class, INFO);
//        set(DriveForwardForSetTimeCommand.class, INFO);
//        set(AutonomousAbstractCommandGroup.class, INFO);
//        set(DriveSetDistanceCommand.class, INFO);
//        set(DrivePidRotateCommand.class, WARN);


        // LIBRARIES
        setPackage(Reflections.class, WARN);

//        set(Joysticks.class, INFO);
//        set(ButtonAssignments.class, INFO);~


        loggerLevels = levelSettingsMapBuilder.build();
    }




















    //@formatter:on
    private static final ImmutableMap<String, Level> loggerLevels;

    /**
     * Flag that is automatically set based on the {@link #rootLevel} setting such that additional detail is
     * added to the logging output, namely the {@code caller} detail.
     */
    private static final boolean WITH_DETAIL = (rootLevel.equals(DEBUG) || rootLevel.equals(TRACE) || rootLevel.equals(ALL));


    private static final String consolePatternLayoutBASE = "%date{HH:mm:ss.SSS zzz} %-5level [%-5thread] %-40logger{40} - %message";
    private static final String consolePatternLayoutSUFFIX = WITH_DETAIL
                                                             ? "      \t\t==> %caller{1}"
                                                             : "%n";

    private static final String consolePatternLayout = consolePatternLayoutBASE + consolePatternLayoutSUFFIX;


    //Used when reporting status messages to indicate the origin of the message
    @SuppressWarnings("InstantiationOfUtilityClass")
    private static final LogbackProgrammaticConfiguration origin = new LogbackProgrammaticConfiguration();


    private LogbackProgrammaticConfiguration() { }


    private static void set(Class<?> clazz, Level level)
    {
        levelSettingsMapBuilder.put(clazz.getName(), level);
    }


    private static void set(String loggerName, Level level)
    {
        levelSettingsMapBuilder.put(loggerName, level);
    }


    private static void setPackage(Package pkg, Level level)
    {
        levelSettingsMapBuilder.put(pkg.getName(), level);
    }


    private static void setPackage(Class<?> clazz, Level level)
    {
        levelSettingsMapBuilder.put(clazz.getPackage().getName(), level);
    }


    public static void configure()
    {
        try
        {
            final LoggerContext context = getLoggerContext();
            if (context != null)
            {
                reportInfo("Programmatically configuring logging");

                reportDebug("Resetting Context");
                context.reset();

                reportDebug("Creating Console Appender");
                ConsoleAppender<ILoggingEvent> consoleAppender = new ConsoleAppender<>();
                consoleAppender.setContext(context);
                consoleAppender.setName("CONSOLE");

                reportDebug("Creating Pattern Layout");
                PatternLayoutEncoder patternLayout = new PatternLayoutEncoder();
                patternLayout.setContext(context);
                patternLayout.setPattern(consolePatternLayout);
                reportDebug("Starting Pattern Layout");
                patternLayout.start();

                consoleAppender.setEncoder(patternLayout);
                reportDebug("Starting Console Appender");
                consoleAppender.start();

                reportDebug("Getting Root Logger");
                final Logger rootLogger = context.getLogger(Logger.ROOT_LOGGER_NAME);
                rootLogger.addAppender(consoleAppender);
                reportDebug("Setting Root Logger Level to " + rootLevel);
                rootLogger.setLevel(rootLevel);

                reportDebug("Configuring various logging levels");
                if (loggerLevels != null)
                {
                    for (Entry<String, Level> entry : loggerLevels.entrySet())
                    {
                        final String loggerName = entry.getKey();
                        final Level level = entry.getValue();
                        context.getLogger(loggerName).setLevel(level);
                    }
                }
                reportInfo("Programmatic logging configuration has completed");

                //Give time for full initialization
                try {TimeUnit.MILLISECONDS.sleep(100);} catch (InterruptedException ignore) {}

                LogTester.log();
            }
            else
            {
                System.err.println(">>>>ERROR<<<< Could not Programmatically configuring logging since a reference to the LoggerContext could not be obtained.");
            }

        }
        catch (Exception e)
        {
            reportError(e);
        }

    }


    /**
     * Reports/logs an error message, via the Logback Context StatusManager to the console at
     * an 'error' status level.
     *
     * @param msg the message to log
     */
    protected static void reportError(@Nonnull String msg)
    {
        reportError(msg, null);
    }


    /**
     * Reports/logs an exception, via the Logback Context StatusManager to the console at
     * an 'error' status level. A default message, indicating an exception occurred, is used when
     * reporting the exception.
     *
     * @param t the throwable/exception to report
     */
    protected static void reportError(@Nonnull Throwable t)
    {
        reportError("An exception occurred during programmatic logging configuration via the " + LogbackProgrammaticConfiguration.class.getName(), t);
    }


    /**
     * Reports/logs an error message and an exception, via the Logback Context StatusManager
     * to the console at an 'error' status level.
     *
     * @param msg the message to log
     * @param t   the throwable/exception to report; if {@code null} only the message is logged
     */
    protected static void reportError(@Nonnull String msg, @Nullable Throwable t)
    {
        final StringBuilder fullMessage = new StringBuilder("LOGGING CONFIGURATION ERROR: ").append(msg);
        if (t != null)
        {
            fullMessage.append(" :: Exception: ").append(t.toString());
        }
        System.err.println(fullMessage.toString());
        reportStatus(new ErrorStatus(fullMessage.toString(), origin, t));
    }


    /**
     * Reports/logs a warning message, via the Logback Context StatusManager to the console at
     * a 'warn' status level.
     *
     * @param msg the message to log
     */
    protected static void reportWarning(@Nonnull String msg)
    {
        reportWarning(msg, null);
    }


    /**
     * Reports/logs an exception, via the Logback Context StatusManager to the console at
     * a 'warn' status level. A default message, indicating an exception occurred, is used when
     * reporting the exception.
     *
     * @param t the throwable/exception to report
     */
    protected static void reportWarning(@Nonnull Throwable t)
    {
        reportWarning("An exception occurred during programmatic logging configuration via the " + LogbackProgrammaticConfiguration.class.getName(), t);
    }


    /**
     * Reports/logs an warning message and an exception, via the Logback Context StatusManager
     * to the console at the 'warn' status level.
     *
     * @param msg the message to log
     * @param t   the throwable/exception to report; if {@code null} only the message is logged
     */
    protected static void reportWarning(@Nonnull String msg, @Nullable Throwable t)
    {
        final StringBuilder fullMessage = new StringBuilder("LOGGING CONFIGURATION WARNING: ").append(msg);
        if (t != null)
        {
            fullMessage.append(" :: Exception: ").append(t.toString());
        }
        System.out.println(fullMessage);

        reportStatus(new WarnStatus(fullMessage.toString(), origin, t));
    }


    /**
     * Reports/logs an informational message, via the Logback Context StatusManager to the console at
     * an 'info' status level.
     *
     * @param msg the message to log
     */
    protected static void reportInfo(@Nonnull String msg)
    {
        final String fullMessage = "LOGGING CONFIGURATION INFORMATION: " + msg;
        System.out.println(fullMessage);
        reportStatus(new InfoStatus(fullMessage, origin));
    }


    /**
     * Reports/logs a debug message, via the Logback Context StatusManager to the console at
     * an 'debug' status level.
     *
     * @param msg the message to log
     */
    protected static void reportDebug(@Nonnull String msg)
    {
        if (logInitializationDebug)
        {
            final String fullMessage = "LOGGING CONFIGURATION DEBUG INFO:  " + msg;
            System.out.println(fullMessage);
            reportStatus(new InfoStatus(fullMessage, origin));
        }
    }


    /**
     * An internal method used for reporting statuses.
     *
     * @param status the status to report
     */
    protected static void reportStatus(Status status)
    {
        try
        {
            final LoggerContext context = getLoggerContext();
            if (context != null)
            {
                StatusManager sm = context.getStatusManager();
                if (sm != null)
                {
                    sm.add(status);
                }
            }
        }
        catch (Exception e)
        {
            System.err.println("An exception occurred when attempting to report/log a Logback status. Cause details: " + e);
            e.printStackTrace();
        }
    }


    @Nullable
    protected static LoggerContext getLoggerContext()
    {
        try
        {
            // assume SLF4J is bound to logback in the current environment
            return (LoggerContext) LoggerFactory.getILoggerFactory();
        }
        catch (Exception e)
        {
            System.err.println("\">>>>ERROR<<<< Could not get LoggerContext reference. Cause Details: " + e.toString());
            e.printStackTrace();
            return null;
        }
    }
}
