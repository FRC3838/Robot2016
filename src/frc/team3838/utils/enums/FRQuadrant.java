package frc.team3838.utils.enums;

public enum FRQuadrant implements RobotQuadrant
{
    FRONT, REAR;


    @Override
    public String getName()
    {
        return name();
    }


}
