package frc.team3838.utils.enums;

public enum SideQuadrant implements RobotQuadrant
{
    LEFT, RIGHT;


    @Override
    public String getName()
    {
        return name();
    }

}
